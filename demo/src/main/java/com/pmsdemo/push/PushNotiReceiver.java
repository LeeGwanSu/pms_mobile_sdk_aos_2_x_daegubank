package com.pmsdemo.push;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.request.ReadMsg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pmsdemo.activity.SplashActivity;

/**
 * Notibar Click 시 실행되는 class
 * 
 */
public class PushNotiReceiver extends BroadcastReceiver implements IPMSConsts {

	@Override
	public void onReceive (final Context context, Intent intent) {

		PushMsg msg = new PushMsg(intent.getExtras());

		CLog.i("PushNotiReceiver) onReceive "+msg.toString());
		try {
			JSONObject appLink = new JSONObject(msg.data);
			if (appLink.has("l")) {
				Toast.makeText(context, "AppLink : " + appLink.getString("l"), Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONArray reads = new JSONArray();
		reads.put(PMSUtil.getReadParam(msg.msgId));

		new ReadMsg(context).request(reads, null);

		Intent i = new Intent(context, SplashActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		i.putExtras(intent.getExtras());

		context.startActivity(i);
	}
}