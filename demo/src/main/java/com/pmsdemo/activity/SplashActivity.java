package com.pmsdemo.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.OnReceivePushListener;
import com.pms.sdk.PMS;
import com.pms.sdk.PMSPopup;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.api.request.LoginPms;
import com.pms.sdk.api.request.NewMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSPopupUtil.btnEventListener;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pmsdemo.R;
import com.pmsdemo.common.IPMSDEMOConts;
import com.pmsdemo.common.util.DataUtil;
import com.pmsdemo.fragment.MessageDetailFragment;
import com.pmsdemo.fragment.MessageListFragment;
import com.pmsdemo.fragment.SettingFragment;
import com.pmsdemo.push.PushNotiReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

//import com.pms.sdk.common.util.PMSPopupUtil.touchEventListener;
//import com.pmsdemo.fragment.SettingFragment;

public class SplashActivity extends FragmentActivity implements Serializable, IPMSConsts, IPMSDEMOConts {

    private static final long serialVersionUID = 1L;

    private static final Handler mHandler = new Handler();
    private static int mLayoutId = 0;
    private static Boolean mbEditState = false;
    public EditText metLoginText = null;
    public EditText metCustNameText = null;
    public EditText metPhoneNumberText = null;
    public EditText metBirthdayText = null;
    public EditText metLocation1Text = null;
    public EditText metLocation2Text = null;
    public EditText metData1Text = null;
    public EditText metData2Text = null;
    public EditText metData3Text = null;

    public RadioGroup mRadioLayout = null;
    public String mstrGender = "";
    transient OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case 1:
                    mstrGender = "M";
                    break;

                case 2:
                    mstrGender = "W";
                    break;
            }
        }
    };
    private transient Context mCon = null;
    private Dialog mDialog = null;
    private transient Prefs mPrefs = null;
    private transient PMS mPms = null;
    private PMSPopup mPmsPopup = null;
    private final btnEventListener btnEvent = new btnEventListener() {
        private static final long serialVersionUID = 1L;

        @Override
        public void response() {
            mPmsPopup.getActivity().finish();
        }
    };
    private final btnEventListener btnEvent1 = new btnEventListener() {
        private static final long serialVersionUID = 1L;

        @Override
        public void response() {
            mPmsPopup.startNotiReceiver();
        }
    };
    private String mstrCustId = "";
//	private SettingFragment mSetting = null;
    private JSONObject mJoUserData = null;
    private MessageListFragment mMsgList = null;
    private MessageDetailFragment mMsgDetail = null;

    transient DialogInterface.OnClickListener loginDialogClickListener = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case AlertDialog.BUTTON_POSITIVE:
                    startInit();
                    break;

                case AlertDialog.BUTTON_NEGATIVE:
                    try {
                        mJoUserData = new JSONObject();
                        mJoUserData.put("custName", metCustNameText.getText().toString());
                        mJoUserData.put("phoneNumber", metPhoneNumberText.getText().toString());
                        mJoUserData.put("birthday", metBirthdayText.getText().toString());
                        mJoUserData.put("location1", metLocation1Text.getText().toString());
                        mJoUserData.put("location2", metLocation2Text.getText().toString());
                        mJoUserData.put("gender", mstrGender);
                        mJoUserData.put("data1", metData1Text.getText().toString());
                        mJoUserData.put("data2", metData2Text.getText().toString());
                        mJoUserData.put("data3", metData3Text.getText().toString());

                        mstrCustId = metLoginText.getText().toString();

                        if (DataUtil.checkParameter(mJoUserData)) {
                            mPrefs.putString("userData", mJoUserData.toString());
                        } else {
                            mJoUserData = null;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startInit();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        actionHide();

        mCon = this;
        int permissionCheck = ContextCompat.checkSelfPermission(mCon, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {

        } else {
            CLog.i("deviceCert:UUID is null, sleep(2000)");
            ActivityCompat.requestPermissions((Activity) mCon, new String[] { Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }
        mPrefs = new Prefs(mCon);

        pmsSetting();

        if (mPms.getCustId().equals("")) {
            settingDialogItems();
            DataUtil.loginDialog(mCon, false, loginDialogClickListener, mRadioLayout, metLoginText, metCustNameText, metPhoneNumberText,
                    metBirthdayText, metLocation1Text, metLocation2Text, metData1Text, metData2Text, metData3Text);
        } else {
            startInit();
        }

//        mPms.setNotiStackEnable(true);
//        Date currentDate = new Date();
//        currentDate.setHours(2);
//        currentDate.setMinutes(0);
//        Date beforeDate = new Date();
//        beforeDate.setHours(currentDate.getHours()-1);
//        beforeDate.setMinutes(currentDate.getMinutes());
//        Date afterDate = new Date();
//        afterDate.setHours(currentDate.getHours());
//        afterDate.setMinutes(currentDate.getMinutes());
//        boolean isDisturb = DateUtil.isDoNotDisturb(beforeDate, afterDate, currentDate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, BTN_LOGIN, Menu.NONE, mCon.getString(R.string.btn_login));
        menu.add(0, BTN_EDIT, Menu.NONE, mCon.getString(R.string.btn_edit));
        menu.add(1, BTN_ALL_SELECT, Menu.NONE, mCon.getString(R.string.btn_all_select));
        menu.add(1, BTN_DEL, Menu.NONE, mCon.getString(R.string.btn_del));
        menu.add(1, BTN_CANCEL, Menu.NONE, mCon.getString(R.string.btn_cancel));
        menu.add(0, BTN_SETCONFIG, Menu.NONE, "설정");
        return true;
    }

    ;

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mPms.getCustId().equals("") == false) {
            menu.findItem(BTN_LOGIN).setTitle(mCon.getString(R.string.btn_loginout));
        } else {
            menu.findItem(BTN_LOGIN).setTitle(mCon.getString(R.string.btn_login));
        }

        if (VIEW_FLAG.MSG_LIST.id == mLayoutId) {
            if (mbEditState == false) {
                menu.setGroupVisible(1, false);
                menu.setGroupVisible(0, true);
            } else if (mbEditState == true) {
                menu.setGroupVisible(1, true);
                menu.setGroupVisible(0, false);
            }
        } else if (VIEW_FLAG.MSG_DETAIL.id == mLayoutId) {
            menu.setGroupVisible(1, false);
            menu.setGroupVisible(0, false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case BTN_LOGIN:
                mMsgList.onBtnClickListener(BTN_LOGIN);
                break;
            case BTN_EDIT:
                mbEditState = true;
                mMsgList.onBtnClickListener(BTN_EDIT);
                initMenuItem();
                setActionBarTitle(mCon.getString(R.string.top_title_2));
                break;
            case BTN_ALL_SELECT:
                mMsgList.onBtnClickListener(BTN_ALL_SELECT);
                break;
            case BTN_DEL:
                mMsgList.onBtnClickListener(BTN_DEL);
                mbEditState = false;
                initMenuItem();
                setActionBarTitle(mCon.getString(R.string.app_name));
                break;
            case BTN_CANCEL:
                mbEditState = false;
                mMsgList.onBtnClickListener(BTN_CANCEL);
                initMenuItem();
                setActionBarTitle(mCon.getString(R.string.app_name));
                break;
            case android.R.id.home:
                mLayoutId = VIEW_FLAG.MSG_LIST.id;
                getSupportFragmentManager().popBackStack();
                IsBackBtn(false);
                initMenuItem();
                setFragment(mLayoutId);
                setActionBarTitle(mCon.getString(R.string.app_name));
                return true;
            case BTN_SETCONFIG:
//                new SetConfig(mCon).request("Y","Y",null);
//                PMSUtil.setMQTTFlag(mCon, true);
//                PMS.getInstance(mCon).setPushReceiverClass(PushDaeguReceiver.class.getName());
                setFragment(VIEW_FLAG.MSG_SETTING.id);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PMS.clear();
    }

    private void startInit() {

        try {
            ProgressBar pro = new ProgressBar(mCon);
            mDialog = new Dialog(mCon, R.style.CustomDialog);
            mDialog.addContentView(pro, new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                    android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
            mDialog.show();
        } catch (Exception e) {
            mDialog = null;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        new DeviceCert(mCon).request(null, new APICallback() {
                            @Override
                            public void response(String code, JSONObject json) {
                                if (CODE_SUCCESS.equals(code)) {
                                    if (StringUtil.isEmpty(mstrCustId) == false) {
                                        new LoginPms(mCon).request(mstrCustId, mJoUserData, new APICallback() {
                                            @Override
                                            public void response(String code, JSONObject json) {
                                                if (CODE_SUCCESS.equals(code)) {
                                                    getNewMsg();
                                                } else {
                                                    Toast.makeText(mCon, mCon.getString(R.string.toast_msg_1) + " Login" + code, Toast.LENGTH_SHORT).show();
                                                    if (mDialog != null) {
                                                        mDialog.dismiss();
                                                        mDialog = null;
                                                    }
                                                    setFragment(VIEW_FLAG.MSG_LIST.id);
                                                }
                                            }
                                        });
                                    } else {
                                        getNewMsg();
                                    }
                                } else {
                                    Toast.makeText(mCon, mCon.getString(R.string.toast_msg_1) + " DeviceCert" + code, Toast.LENGTH_SHORT).show();
                                    if (mDialog != null) {
                                        mDialog.dismiss();
                                        mDialog = null;
                                    }

                                    setFragment(VIEW_FLAG.MSG_LIST.id);
                                }
                            }
                        });
                    }
                });
            }
        }).start();
    }

    private void getNewMsg() {
        String type = "";
        String msgid = "";

        if (mPrefs.getBoolean(PREF_APP_FIRST)) {
            type = TYPE_NEXT;
            msgid = mPrefs.getString(PREF_MAX_USER_MSG_ID);
            if (StringUtil.isEmpty(msgid)) msgid = "-1";
            if (msgid.equals("0") || msgid.equals("-1")) {
                type = TYPE_PREV;
                msgid = "-1";
            }
        } else {
            type = TYPE_PREV;
            msgid = "-1";
        }
        CLog.i("getLastUserMsgId" + mPms.getLastUserMsgId());

        new NewMsg(mCon).request(type, mPms.getLastUserMsgId(), "-1", "1", PAGE_ROW, new APICallback() {
            @Override
            public void response(String code, JSONObject json) {
                if (CODE_SUCCESS.equals(code)) {
                    mPrefs.putBoolean(PREF_APP_FIRST, true);
                    Toast.makeText(mCon, mCon.getString(R.string.toast_msg_2), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mCon, mCon.getString(R.string.toast_msg_1) + " NewMsg" + code, Toast.LENGTH_SHORT).show();
                }

                if (mDialog != null) {
                    mDialog.dismiss();
                    mDialog = null;
                }

                setFragment(VIEW_FLAG.MSG_LIST.id);
            }
        });
    }

    private void pmsSetting() {

        mPms = PMS.getInstance(mCon, "997255892867");
        mPms.setPopupSetting(true, "DGB");
        mPms.setPopupNoti(true);
        mPms.setDebugTAG("DGB");
        mPms.setDebugMode(true);
        mPms.setRingMode(true);
        mPms.setVibeMode(true);
        mPms.setScreenWakeup(true);
        mPms.setNotiOrPopup(Boolean.FALSE);
        mPms.setPopupNoti(true);
        mPms.setIsPopupActivity(true);
        mPms.startMQTTService(mCon); // Private 서버 사용시 주석 해제

        mPms.setAllowedTime(true);
        mPms.setFirstTime("090000");
        mPms.setSecondTime("110000");

        pmsPopupSetting();
        mPms.setOnReceivePushListener(new OnReceivePushListener() {
            @Override
            public boolean onReceive(Context context, Bundle bundle)
            {
                CLog.i("OnReceivePushListener");
                return true;
            }
        });
        mPms.setNotiStackEnable(true);
        mPms.getLastUserMsgId();
        mPms.selectAllOfMsg();
        mPms.selectAllOfMsgGrp();
        mPms.selectAllOfMsgByMsgGrpCode("C000");
        mPms.setNotificationClickReceiver("action", PushNotiReceiver.class);
//        PMSDB.getInstance(getApplicationContext()).deleteAll();
//        int unreadMsgCount = PMSDB.getInstance(getApplicationContext()).selectNewMsgCnt();
//        boolean isDoNotDisturb = DateUtil.isDoNotDisturb(from, to current);
        mPrefs.putString(PREF_NOTI_FLAG, IPMSConsts.FLAG_Y);
//        mPms.sendGoBackNoti(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            mPms.setNotificationChannelInfo("알림채널","소리가 나는 채널입니다");
            mPms.setDoNotDisturbChannelInfo("무음채널","방해금지시간 적용시 소리가 나지 않는 채널입니다");
        }
    }

    private void pmsPopupSetting() {
        mPmsPopup = PMS.getPopUpInstance();
        mPmsPopup.setXmlAndDefaultFlag(true);

        mPmsPopup.setLayoutXMLTextResId("pms_text_popup");
        mPmsPopup.setXMLTextButtonType("LinearLayout", "LinearLayout");
        mPmsPopup.setXMLTextButtonTagName("button1", "button2");

        mPmsPopup.setLayoutXMLRichResId("pms_rich_popup");
        mPmsPopup.setXMLRichButtonType("ImageView");
        mPmsPopup.setXMLRichButtonTagName("button1");

        mPmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
        mPmsPopup.setRichBottomBtnClickListener(btnEvent);

        mPmsPopup.commit();
    }

    public void setFragment(int id, Object... args) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (id == VIEW_FLAG.MSG_LIST.id) {
            mMsgList = MessageListFragment.newInstance();
            ft.replace(R.id.sub_list_layout, mMsgList, id + "");
            mLayoutId = id;
            IsBackBtn(false);
        }
        else if (id == VIEW_FLAG.MSG_DETAIL.id) {
            mMsgDetail = MessageDetailFragment.newInstance(args);
            ft.replace(R.id.sub_Detail_layout, mMsgDetail, id + "");
            ft.hide(mMsgList);
            mLayoutId = id;
            IsBackBtn(true);
        }
        else if(id == VIEW_FLAG.MSG_SETTING.id)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.sub_list_layout, new SettingFragment(), VIEW_FLAG.MSG_SETTING.id+"").commit();
            mLayoutId = id;
            IsBackBtn(true);
        }


        if (id != VIEW_FLAG.MSG_LIST.id) {
            ft.addToBackStack(id + "");
        }

        ft.commitAllowingStateLoss();
    }

    public void settingDialogItems() {
        mRadioLayout = new RadioGroup(mCon);
        mRadioLayout.setOrientation(RadioGroup.HORIZONTAL);
        mRadioLayout.setOnCheckedChangeListener(checkedChangeListener);

        RadioButton rb1 = new RadioButton(mCon);
        rb1.setText(mCon.getString(R.string.radio_man));

        RadioButton rb2 = new RadioButton(mCon);
        rb2.setText(mCon.getString(R.string.radio_woman));

        mRadioLayout.addView(rb1);
        mRadioLayout.addView(rb2);

        metLoginText = new EditText(mCon);
        metCustNameText = new EditText(mCon);
        metPhoneNumberText = new EditText(mCon);
        metBirthdayText = new EditText(mCon);
        metLocation1Text = new EditText(mCon);
        metLocation2Text = new EditText(mCon);
        metData1Text = new EditText(mCon);
        metData2Text = new EditText(mCon);
        metData3Text = new EditText(mCon);

        metLoginText.setHint(mCon.getString(R.string.edit_hint_1));
        metCustNameText.setHint(mCon.getString(R.string.edit_hint_2));
        metPhoneNumberText.setHint(mCon.getString(R.string.edit_hint_3));
        metBirthdayText.setHint(mCon.getString(R.string.edit_hint_4));
        metLocation1Text.setHint(mCon.getString(R.string.edit_hint_5));
        metLocation2Text.setHint(mCon.getString(R.string.edit_hint_6));
        metData1Text.setHint(mCon.getString(R.string.edit_hint_7));
        metData2Text.setHint(mCon.getString(R.string.edit_hint_8));
        metData3Text.setHint(mCon.getString(R.string.edit_hint_9));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setActionBarTitle(String title) {
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setTitle(title);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void IsBackBtn(Boolean isBtn) {
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(isBtn);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void initMenuItem() {
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void actionShow() {
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().show();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void actionHide() {
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().hide();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.support.v4.app.FragmentActivity#onKeyDown(int, android.view.KeyEvent) Back key에 대한 이벤트 처리 Method
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 각 Page Back Button에 대한 이벤트
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                mLayoutId = VIEW_FLAG.MSG_LIST.id;
                getSupportFragmentManager().popBackStack();
                setFragment(mLayoutId);
                IsBackBtn(false);
                initMenuItem();
                setActionBarTitle(mCon.getString(R.string.app_name));
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mCon);
                alertDialog.setTitle(mCon.getString(R.string.dialog_title_1));
                alertDialog.setMessage(mCon.getString(R.string.dialog_content_1));
                alertDialog.setNegativeButton(mCon.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                });
                alertDialog.setPositiveButton(mCon.getString(R.string.btn_no), null);
                alertDialog.show();
            }
        }
        return false;
    }

    public enum VIEW_FLAG {
        MSG_LIST(0x10), MSG_DETAIL(0x21), MSG_SETTING(0x22);

        public int id;

        private VIEW_FLAG(int id) {
            this.id = id;
        }
    }
}
