package com.pmsdemo.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.api.request.LoginPms;
import com.pms.sdk.api.request.LogoutPms;
import com.pms.sdk.api.request.NewMsg;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pmsdemo.R;
import com.pmsdemo.activity.SplashActivity;
import com.pmsdemo.common.util.DataUtil;
import com.pmsdemo.data.MessageListAdapter;

public class MessageListFragment extends BaseFragment {

	private static final Handler mHandler = new Handler();

	private Dialog mDialog = null;

	private AlertDialog.Builder mDialogDel = null;

	private View mView = null;

	private Boolean mbAllSelectState = false;
	private Boolean mbEditState = false;

	private Cursor mMsgCursor = null;

	private ListView mMainListView = null;

	private TextView mNewMsgCountText = null;

	private PullToRefreshListView mPullRefreshListView = null; // Pull Refresh Instance

	private MessageListAdapter mAdapter = null;

	private final ArrayList<String> mRowSelectArray = new ArrayList<String>();

	public static MessageListFragment newInstance () {
		MessageListFragment mlf = new MessageListFragment();
		return mlf;
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.message_list_layout, null);

		preformDel();

		mNewMsgCountText = (TextView) mView.findViewById(R.id.new_msg_count);

		mPullRefreshListView = (PullToRefreshListView) mView.findViewById(R.id.msg_list);
		mMainListView = mPullRefreshListView.getRefreshableView();

		mCon.registerReceiver(msgGrpReceiver, new IntentFilter(RECEIVER_LIST_REQUERY));
		mCon.registerReceiver(newMsgReceiver, new IntentFilter(RECEIVER_PUSH));

		return mView;
	}

	@Override
	public void onActivityCreated (Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		((SplashActivity) getActivity()).actionShow();

		mMainListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mMainListView.setDividerHeight(0);

		mPullRefreshListView.setOnRefreshListener(onRefreshListener);

		startInit(true);
	}

	@Override
	public void onDestroy () {
		super.onDestroy();
		try {
			mCon.unregisterReceiver(msgGrpReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	private void startInit (final Boolean state) {
		if (mDialog == null) {
			try {
				ProgressBar pro = new ProgressBar(mCon);
				mDialog = new Dialog(mCon, R.style.CustomDialog);
				mDialog.addContentView(pro, new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				mDialog.show();
			} catch (Exception e) {
				mDialog = null;
			}
		}

		new Thread(new Runnable() {
			@Override
			public void run () {
				mHandler.post(new Runnable() {
					@Override
					public void run () {
						setMsgList(state);
						if (mDialog != null) {
							mDialog.dismiss();
							mDialog = null;
						}
					}
				});
			}
		}).start();

	}

	private void setMsgList (Boolean state) {
		if (mMsgCursor != null) {
			mMsgCursor.close();
		}

		mMsgCursor = mPms.selectMsgList(DB_PAGE, DB_ROW);
//		mNewMsgCountText.setText(String.format(mCon.getString(R.string.content_msg1), mPms.selectNewMsgCnt()));

		if (state) {
			mAdapter = new MessageListAdapter(mCon, mMsgCursor, true);
			mMainListView.setAdapter(mAdapter);
		} else {
			mAdapter.swapCursor(mMsgCursor);
		}

		if (mbEditState == false) {
			mAdapter.hmCheckBox.clear();
			mAdapter.hashMapSetting(mMsgCursor);
		}
	}

	private void loginState () {
		try {
			ProgressBar pro = new ProgressBar(mCon);
			mDialog = new Dialog(mCon, R.style.CustomDialog);
			mDialog.addContentView(pro, new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
			mDialog.show();
		} catch (Exception e) {
			mDialog = null;
		}

		new Thread(new Runnable() {
			@Override
			public void run () {
				mHandler.post(new Runnable() {
					@Override
					public void run () {
						if (PMSUtil.getCustId(mCon).equals("")) {
							((SplashActivity) getActivity()).settingDialogItems();
							DataUtil.loginDialog(mCon, true, loginDialogClickListener, ((SplashActivity) getActivity()).mRadioLayout,
									((SplashActivity) getActivity()).metLoginText, ((SplashActivity) getActivity()).metCustNameText,
									((SplashActivity) getActivity()).metPhoneNumberText, ((SplashActivity) getActivity()).metBirthdayText,
									((SplashActivity) getActivity()).metLocation1Text, ((SplashActivity) getActivity()).metLocation2Text,
									((SplashActivity) getActivity()).metData1Text, ((SplashActivity) getActivity()).metData2Text,
									((SplashActivity) getActivity()).metData3Text);
						} else {
							new LogoutPms(mCon).request(new APICallback() {
								@Override
								public void response (String code, JSONObject json) {
									mPref.putString("userData", "");
									startInit(false);
								}
							});
						}
					}
				});
			}
		}).start();
	}

	private void preformDel () {
		mDialogDel = new AlertDialog.Builder(mCon);
		mDialogDel.setTitle(mCon.getString(R.string.dialog_title_2));
		mDialogDel.setMessage(mCon.getString(R.string.dialog_content_4));
		mDialogDel.setNegativeButton(mCon.getString(R.string.btn_cancel), onDeleteListener);
		mDialogDel.setPositiveButton(mCon.getString(R.string.btn_confirm), onDeleteListener);
	}

	private final BroadcastReceiver msgGrpReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive (Context context, Intent intent) {
			setMsgList(false);
//			mNewMsgCountText.setText(String.format(mCon.getString(R.string.content_msg1), mPms.selectNewMsgCnt()));
		}
	};

	private final BroadcastReceiver newMsgReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive (Context context, Intent intent) {
			String strMsgID = mPref.getString(PREF_MAX_USER_MSG_ID);
			if (StringUtil.isEmpty(strMsgID)) strMsgID = "-1";
			new NewMsg(mCon).request(TYPE_NEXT, strMsgID, "-1", "1", PAGE_ROW, new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						mHandler.post(new Runnable() {
							@Override
							public void run () {
								startInit(false);
							}
						});
					}
				}
			});
		}
	};

	public void onBtnClickListener (int id) {
		switch (id) {
			case BTN_LOGIN:
				loginState();
				break;
			case BTN_EDIT:
				mbEditState = true;
				mAdapter.setCheckState(true);
				break;
			case BTN_ALL_SELECT:
				if (mbAllSelectState == false) {
					mAdapter.hashMapInit(true);
					mbAllSelectState = true;
				} else {
					mAdapter.hashMapInit(false);
					mbAllSelectState = false;
				}
				mAdapter.notifyDataSetChanged();
				break;
			case BTN_DEL:
				getHashMapData(mAdapter.hmCheckBox);
				if (mRowSelectArray.size() != 0) {
					mDialogDel.show();
				} else {
					Toast.makeText(mCon, mCon.getString(R.string.toast_msg_5), Toast.LENGTH_SHORT).show();
					mAdapter.setCheckState(false);
				}
				break;
			case BTN_CANCEL:
				mAdapter.setCheckState(false);
				mAdapter.hashMapInit(false);
				mAdapter.notifyDataSetChanged();
				mbAllSelectState = false;
				mbEditState = false;
				break;
		}
	}

	OnClickListener btnClickListener = new OnClickListener() {
		@Override
		public void onClick (View v) {
		}
	};

	DialogInterface.OnClickListener loginDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick (DialogInterface dialog, int which) {
			switch (which) {
				case AlertDialog.BUTTON_NEGATIVE:
					JSONObject addDate = null;
					try {
						addDate = new JSONObject();
						addDate.put("custName", ((SplashActivity) getActivity()).metCustNameText.getText().toString());
						addDate.put("phoneNumber", ((SplashActivity) getActivity()).metPhoneNumberText.getText().toString());
						addDate.put("birthday", ((SplashActivity) getActivity()).metBirthdayText.getText().toString());
						addDate.put("location1", ((SplashActivity) getActivity()).metLocation1Text.getText().toString());
						addDate.put("location2", ((SplashActivity) getActivity()).metLocation2Text.getText().toString());
						addDate.put("gender", ((SplashActivity) getActivity()).mstrGender);
						addDate.put("data1", ((SplashActivity) getActivity()).metData1Text.getText().toString());
						addDate.put("data2", ((SplashActivity) getActivity()).metData2Text.getText().toString());
						addDate.put("data3", ((SplashActivity) getActivity()).metData3Text.getText().toString());

						if (DataUtil.checkParameter(addDate)) {
							mPref.putString("userData", addDate.toString());
						} else {
							addDate = null;
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

					new LoginPms(mCon).request(((SplashActivity) getActivity()).metLoginText.getText().toString(), addDate, new APICallback() {
						@Override
						public void response (String code, JSONObject json) {
							if (CODE_SUCCESS.equals(code)) {
								new NewMsg(mCon).request(TYPE_PREV, "-1", "-1", "1", PAGE_ROW, new APICallback() {
									@Override
									public void response (String code, JSONObject json) {
										if (CODE_SUCCESS.equals(code)) {
											startInit(false);
										}
									}
								});
							}
						}
					});
					break;

				case AlertDialog.BUTTON_POSITIVE:
					Toast.makeText(mCon, mCon.getString(R.string.toast_msg_3), Toast.LENGTH_SHORT).show();
					new DeviceCert(mCon).request(null, new APICallback() {
						@Override
						public void response (String code, JSONObject json) {
							if (CODE_SUCCESS.equals(code)) {
								try
								{
									String unreadMsgCount = json.getString("newMsgCnt");
								}
								catch (JSONException e)
								{
									e.printStackTrace();
								}
								new NewMsg(mCon).request(TYPE_PREV, "-1", "-1", "1", PAGE_ROW, new APICallback() {
									@Override
									public void response (String code, JSONObject json) {
										if (CODE_SUCCESS.equals(code)) {
											startInit(false);
										}
									}
								});
							}
						}
					});
					break;
			}
		}
	};

	@SuppressWarnings("rawtypes")
	private void getHashMapData (HashMap<String, Boolean> map) {
		Set key = map.keySet();
		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			String keyName = (String) iterator.next();
			Boolean valueName = map.get(keyName);
			if (valueName) {
				mRowSelectArray.add(keyName);
			}
		}
	}

	private final DialogInterface.OnClickListener onDeleteListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick (DialogInterface dialog, int which) {
			switch (which) {
				case AlertDialog.BUTTON_NEGATIVE: // 취소버튼
					mRowSelectArray.clear();
					onBtnClickListener(BTN_CANCEL);
					break;
				case AlertDialog.BUTTON_POSITIVE: // 확인버튼
					// 저장된 값 DB에서 삭제
					for (int i = 0; i < mRowSelectArray.size(); i++) {
						mPms.deleteUserMsgId(mRowSelectArray.get(i));
						mAdapter.hmCheckBox.remove(mRowSelectArray.get(i));
					}
					mRowSelectArray.clear(); // 삭제한 값 초기화
					startInit(false); // List 갱신
					onBtnClickListener(BTN_CANCEL);
					break;
			}
		}
	};

	private final OnRefreshListener onRefreshListener = new OnRefreshListener() {
		@Override
		public void onRefresh () {
			if (mbEditState == false) {
				String strMsgID = mPref.getString(PREF_MAX_USER_MSG_ID);
				if (StringUtil.isEmpty(strMsgID)) strMsgID = "-1";
				new NewMsg(mCon).request(TYPE_NEXT, strMsgID, "-1", "1", PAGE_ROW, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							mHandler.post(new Runnable() {
								@Override
								public void run () {
									startInit(false);
								}
							});
						}

						mPullRefreshListView.onRefreshComplete(); // 이벤트 완료
					}
				});
			} else {
				Toast.makeText(mCon, mCon.getString(R.string.toast_msg_4), Toast.LENGTH_SHORT).show();
				mPullRefreshListView.onRefreshComplete(); // 이벤트 완료
			}
		}
	};
}
