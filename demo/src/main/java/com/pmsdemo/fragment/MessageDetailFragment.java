package com.pmsdemo.fragment;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.ReadMsg;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.push.PMSWebViewBridge;
import com.pmsdemo.R;
import com.pmsdemo.activity.SplashActivity;

public class MessageDetailFragment extends BaseFragment {

	private static final String APP_SCHEME = "pms:";

	private static final Handler mHandler = new Handler();

	private View mView = null;

	private TextView mTitleText = null;
	private TextView mContentText = null;
	private TextView mDateText = null;

	private LinearLayout mMainText = null;

	private RelativeLayout mMainWebview = null;

	private WebView mWebView = null;

	private ProgressBar mProgressbar = null;

	private Dialog mDialog = null;

	private String mstrMsgUserId = "";
	private String mstrMsgType = "";
	private String mstrMsgTitle = "";

	public static MessageDetailFragment newInstance (Object... args) {
		MessageDetailFragment mdf = new MessageDetailFragment();
		mdf.setMsgUserId((String) args[0]);
		mdf.setMsgType((String) args[1]);
		mdf.setMsgTitle((String) args[2]);
		return mdf;
	}

	private void setMsgUserId (String userid) {
		mstrMsgUserId = userid;
	}

	private void setMsgType (String msgtype) {
		mstrMsgType = msgtype;
	}

	private void setMsgTitle (String msgtitle) {
		mstrMsgTitle = msgtitle;
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.message_detail_layout, null);

		((SplashActivity) getActivity()).setActionBarTitle(mstrMsgTitle);
		mTitleText = (TextView) mView.findViewById(R.id.detail_title_text);
		mContentText = (TextView) mView.findViewById(R.id.detail_content_text);
		mDateText = (TextView) mView.findViewById(R.id.detail_text_date);

		mMainText = (LinearLayout) mView.findViewById(R.id.detail_text_layout);
		mMainWebview = (RelativeLayout) mView.findViewById(R.id.detail_webview_layout);

		mWebView = (WebView) mView.findViewById(R.id.detail_content_webView);

		mProgressbar = (ProgressBar) mView.findViewById(R.id.detail_content_prg);

		return mView;
	}

	@Override
	public void onActivityCreated (Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (mstrMsgType.equals("T")) {
			mMainText.setVisibility(View.VISIBLE);
			mMainWebview.setVisibility(View.GONE);
		} else if (mstrMsgType.equals("H") || mstrMsgType.equals("l")) {
			mMainText.setVisibility(View.GONE);
			mMainWebview.setVisibility(View.VISIBLE);
		}

		startInit();
	}

	private void startInit () {
		try {
			ProgressBar pro = new ProgressBar(mCon);
			mDialog = new Dialog(mCon, R.style.CustomDialog);
			mDialog.addContentView(pro, new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
			mDialog.show();
		} catch (Exception e) {
			mDialog = null;
		}

		new Thread(new Runnable() {
			@Override
			public void run () {
				mHandler.post(new Runnable() {
					@Override
					public void run () {
						setMsgList();
						if (mDialog != null) {
							mDialog.dismiss();
							mDialog = null;
						}
					}
				});
			}
		}).start();
	}

	private void setMsgList () {
		Msg msg = mPms.selectMsgWhereUserMsgId(mstrMsgUserId);

		if (msg.readYn.equals(FLAG_N)) {
			JSONArray reads = new JSONArray();
			reads.put(PMSUtil.getReadParam(msg.msgId));

			new ReadMsg(mCon).request(reads, new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						mCon.sendBroadcast(new Intent(RECEIVER_LIST_REQUERY)); // List 갱신
					}
				}
			});
		}

		String date = DateUtil.convertDate(msg.regDate, "yyyyMMddkkmmss", "yyyy/MM/dd kk:mm");
		mDateText.setText(date);

		if (mstrMsgType.equals("T")) {
			mTitleText.setText(mstrMsgTitle);
			mContentText.setText(msg.msgText);
		} else if (mstrMsgType.equals("H") || mstrMsgType.equals("l")) {
			webViewSetting(msg);
		}

	}

	@SuppressWarnings("deprecation")
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface", "ClickableViewAccessibility" })
	private void webViewSetting (final Msg msg) {

		mWebView.clearCache(true);
		mWebView.clearHistory();
		mWebView.clearFormData();

		mWebView.setBackgroundColor(android.R.style.Theme_Translucent);
		mWebView.setHorizontalScrollBarEnabled(false);
		mWebView.setVerticalScrollBarEnabled(false);

		WebSettings settings = mWebView.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setSupportMultipleWindows(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setGeolocationEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setUseWideViewPort(true);
		settings.setPluginState(PluginState.ON);

		mWebView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading (WebView view, String url) {
				CLog.e("webViewClient:url=" + url);
				if (url.startsWith(APP_SCHEME)) {
					return true;
				}
				return false;
			}
		});

		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged (WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				mProgressbar.setProgress(newProgress);
				if (newProgress >= 100) {
					mProgressbar.setVisibility(View.GONE);
				}
			}
		});

		mWebView.addJavascriptInterface(new PMSWebViewBridge(mCon), "pms");

		((SplashActivity) mCon).runOnUiThread(new Runnable() {
			@Override
			public void run () {
				if (Msg.TYPE_L.equals(msg.msgType) && msg.msgText.startsWith("http")) {
					mWebView.loadUrl(msg.msgText);
				} else {
					mWebView.loadDataWithBaseURL("pms://pms.humuson.com/", changeUrl(msg.msgText), "text/html", "utf-8", null);
				}
			}
		});

		mWebView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch (View v, MotionEvent event) {
				if (event.getAction() != MotionEvent.ACTION_MOVE) {
					((WebView) v).setWebViewClient(new WebViewClient() {
						@Override
						public boolean shouldOverrideUrlLoading (WebView view, String url) {
							CLog.i("webViewClient:url=" + url);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							Uri uri = Uri.parse(url);
							intent.setData(uri);
							startActivity(intent);
							return true;
						}
					});
				}
				return false;
			}
		});
	}

	private String changeUrl (String url) {
		String temp = "";
		int ver = Build.VERSION.SDK_INT;
		if (ver > Build.VERSION_CODES.JELLY_BEAN) {
			int len = url.indexOf("target-densitydpi");
			if (len != -1) {
				temp = url.replace(", target-densitydpi=device-dpi", "");
			} else {
				temp = url;
			}
		} else {
			temp = url;
		}
		return temp;
	}
}
