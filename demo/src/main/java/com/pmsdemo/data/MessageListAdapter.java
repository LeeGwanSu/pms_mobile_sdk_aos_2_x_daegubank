package com.pmsdemo.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.DateUtil;
import com.pmsdemo.R;
import com.pmsdemo.activity.SplashActivity;
import com.pmsdemo.activity.SplashActivity.VIEW_FLAG;

public class MessageListAdapter extends CursorAdapter implements IPMSConsts {

	private Context mCon = null;

	private Boolean mbCheckBoxState = false;

	public HashMap<String, Boolean> hmCheckBox = new HashMap<String, Boolean>();

	public MessageListAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
		mCon = context;
	}

	@Override
	public void bindView (View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();

		Msg msg = new Msg(cursor);

		holder.mTitleText.setText(msg.pushTitle);

		holder.mContentText.setText(msg.pushMsg);

		holder.mMainLayout.setTag(cursor.getPosition());
		if (hmCheckBox.containsKey(msg.userMsgId)) {
			if (hmCheckBox.get(msg.userMsgId)) {
				holder.mMainLayout.setBackgroundColor(Color.rgb(220, 220, 220));
			} else {
				holder.mMainLayout.setBackgroundColor(Color.WHITE);
			}
		} else {
			holder.mMainLayout.setBackgroundColor(Color.WHITE);
		}
		holder.mMainLayout.setOnClickListener(rowClickListener);

		if (msg.readYn.equals(FLAG_N)) {
			((RelativeLayout) view.findViewById(R.id.count_layout)).setVisibility(View.VISIBLE);
			holder.mCountText.setText(msg.readYn);
		} else {
			((RelativeLayout) view.findViewById(R.id.count_layout)).setVisibility(View.GONE);
		}

		if (msg.msgType.equals("H") || msg.msgType.equals("l")) {
			holder.mTypeImgview.setVisibility(View.VISIBLE);
		} else {
			holder.mTypeImgview.setVisibility(View.GONE);
		}

		String date = DateUtil.convertDate(msg.regDate, "yyyyMMddkkmmss", "yyyy/MM/dd kk:mm");
//		String time = date.substring(11);
//		time = getTimeChange(time, "24", "00");
//		holder.mDateText.setText(date.substring(0, 11) + time);
	}

	@Override
	public View newView (Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.message_list_row, parent, false);

		ViewHolder holder = (ViewHolder) view.getTag();
		if (holder == null) {
			holder = new ViewHolder();
			holder.mMainLayout = (LinearLayout) view.findViewById(R.id.row_main);

			holder.mTitleText = (TextView) view.findViewById(R.id.row_title);
			holder.mContentText = (TextView) view.findViewById(R.id.row_content);
			holder.mDateText = (TextView) view.findViewById(R.id.row_date);
			holder.mCountText = (TextView) view.findViewById(R.id.row_count);

			holder.mTypeImgview = (ImageView) view.findViewById(R.id.row_type);

			view.setTag(holder);
		}

		return view;
	}

	public void setCheckState (Boolean state) {
		mbCheckBoxState = state;
	}

	@SuppressWarnings("rawtypes")
	public void hashMapInit (Boolean state) {
		Set key = hmCheckBox.keySet();
		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			String keyName = (String) iterator.next();
			Boolean valueName = hmCheckBox.get(keyName);

			if (valueName != state) {
				hmCheckBox.put(keyName, state);
			}
		}
	}

	public void hashMapSetting (Cursor c) {
		while (c.moveToNext()) {
			Msg msg = new Msg(c);
			hmCheckBox.put(msg.userMsgId, false);
		}
	}

	public void sethashMapDate (Cursor c) {
		while (c.moveToNext()) {
			Msg msg = new Msg(c);
			if (hmCheckBox.containsKey(msg.userMsgId) == false) {
				hmCheckBox.put(msg.userMsgId, false);
			}
		}
	}

	private String getTimeChange (String date, String after, String before) {
		int index = date.indexOf(after);
		if ((index != -1) && (index == 0)) {
			String temp = "";
			temp = date.substring(2);
			date = before + temp;
		}

		return date;
	}

	private final OnClickListener rowClickListener = new OnClickListener() {
		@Override
		public void onClick (View v) {
			int pos = (Integer) v.getTag();
			Cursor c = (SQLiteCursor) getItem(pos);
			Msg msg = new Msg(c);

			if (mbCheckBoxState == false) {
				((SplashActivity) mCon).setFragment(VIEW_FLAG.MSG_DETAIL.id, msg.userMsgId, msg.msgType, msg.pushTitle);
			} else {
				if (hmCheckBox.containsKey(msg.userMsgId)) {
					if (hmCheckBox.get(msg.userMsgId) == false) {
						v.setBackgroundColor(Color.rgb(220, 220, 220));
						hmCheckBox.put(msg.userMsgId, true);
					} else {
						v.setBackgroundColor(Color.WHITE);
						hmCheckBox.put(msg.userMsgId, false);
					}
				} else {
					v.setBackgroundColor(Color.rgb(220, 220, 220));
					hmCheckBox.put(msg.userMsgId, true);
				}
			}
		}
	};

	private static class ViewHolder {
		LinearLayout mMainLayout = null;

		TextView mTitleText = null;
		TextView mContentText = null;
		TextView mDateText = null;
		TextView mCountText = null;

		ImageView mTypeImgview = null;
	}
}
