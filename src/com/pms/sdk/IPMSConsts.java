package com.pms.sdk;

public interface IPMSConsts {
    String PMS_COMPANY = "daegubank";   //대구은행
    String PMS_VERSION = "2.1.4";
    public final static String PMS_VERSION_UPDATE_DATE = "202211251833";

    // [start] GCM
    String KEY_APP = "app";
    String KEY_SENDER = "sender";
    String KEY_GCM_PROJECT_ID = "gcm_project_id";
    String KEY_GCM_TOKEN = "registration_id";

    String URL_GOOGLE_CLIENT = "https://www.google.com/accounts/ClientLogin";
    String URL_GOOGLE_SENDER = "https://android.apis.google.com/c2dm/send";

    String ACTION_RECEIVE = "com.google.android.fcm.intent.RECEIVE";
    String GSF_PACKAGE = "com.google.android.gsf";

    String GCMRECIVER_PATH = "com.pms.sdk.push.gcm.GCMReceiver";
    // [end] GCM

    // [start] MQTT
    String ACTION_START = "MQTT.START";
    public static final String ACTION_RESTART = "MQTT.RESTART";
    String ACTION_FORCE_START = "MQTT.FORCE_START";
    String ACTION_STOP = "MQTT.STOP";
    String ACTION_RECONNECT = "MQTT.RECONNECT";
    String ACTION_CHANGERECONNECT = "MQTT.CANGERECONNECT";
    String ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT";
    String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    String ACTION_ACTION_PACKAGE_RESTARTED = "android.intent.action.ACTION_PACKAGE_RESTARTED";
    // [end] MQTT

    // [start] PUSH
    String KEY_MSG_ID = "i";
    String KEY_NOTI_TITLE = "notiTitle";
    String KEY_NOTI_MSG = "notiMsg";
    String KEY_NOTI_IMG = "notiImg";
    String KEY_MSG = "message";
    String KEY_SOUND = "sound";
    String KEY_MSG_TYPE = "t";
    String KEY_DATA = "d";
    String KEY_COLOR_FLAG = "cF";
    String KEY_TITLE_COLOR = "tC";
    String KEY_CONTENT_COLOR = "cC";
    String KEY_NOT_POPUP = "a";
    // [end] PUSH

	// [start] properties
	public final static String PMS_PROPERY_NAME = "pms.properties";
	public static final String PRO_NOTIFICATION_CLICK_ACTIVITY_ACTION = "push_click_activity_action";
	public static final String PRO_NOTIFICATION_CLICK_ACTIVITY_CLASS = "push_click_activity_class";
	public static final String PRO_NOTIFICATION_CLICK_ACTIVITY_FLAGS = "push_click_activity_flag";
	public static final String PRO_NOTIFICATION_CLICK_ACTIVITY_FLAGS_ENABLE = "push_click_activity_flag_enable";
	public static final String PRO_NOTIFICATION_CLICK_ACTIVITY_USE_BACKSTACK = "push_click_activity_backstack";
	// [end] properties

    // [start] Receiver
    String RECEIVER_PUSH = "com.pms.sdk.push";
    String RECEIVER_REQUERY = "com.pms.sdk.requery";
    // public static final String RECEIVER_REFRESH_MSG_GRP = "com.pms.sdk.refreshmsggrp";
    // public static final String RECEIVER_REFRESH_MSG = "com.pms.sdk.refreshmsg";
    // public static final String RECEIVER_REFRESH_BADGE = "com.pms.sdk.refreshbadge";
    String RECEIVER_CLOSE_INBOX = "com.pms.sdk.closeinbox";
    String RECEIVER_NOTIFICATION = "com.pms.sdk.notification";
    String DEFAULT_PUSH_POPUP_ACTIVITY = "com.pms.sdk.push.PushPopupActivity";
    String RECEIVER_CHANGE_POPUP = "receiver_popup_change";
    // [end] Receiver

    // [start] Preferences
    String PREF_FILE_NAME = "pref_pms";

    String FLAG_Y = "Y";
    String FLAG_N = "N";
    String PROTOCOL_SSL = "S";
    String PROTOCOL_TCP = "T";

    String PREF_CUST_ID = "cust_id";
    String PREF_LOGINED_CUST_ID = "logined_cust_id";
    String PREF_MAX_USER_MSG_ID = "max_user_msg_id";
    String PREF_SSL_SIGN_KEY = "ssl_sign_key";
    String PREF_SSL_SIGN_PASS = "ssl_sign_pass";

    String PREF_MQTT_FLAG = "mqtt_flag";

    String PREF_CLICK_LIST = "click_list";
    String PREF_READ_LIST = "read_list";

    String PREF_MSG_FLAG = "msg_flag";
    String PREF_NOTI_FLAG = "noti_flag";
    String PREF_NOTI_STYLE_STACK = "noti_style_stack";
    String PREF_API_LOG_FLAG = "api_log_flag";
    String PREF_PRIVATE_LOG_FLAG = "private_log_flag";
    String PREF_PRIVATE_FLAG = "private_flag";
    String PREF_ALERT_FLAG = "alert_flag";
    String PREF_RING_FLAG = "ring_flag";
    String PREF_VIBE_FLAG = "vibe_flag";
    String PREF_DUP_FLAG = "dup_flag";
    String PREF_ALLOWED_FLAG = "allowed_flag";
    String PREF_SCREEN_WAKEUP_FLAG = "screen_wakeup_flag";
    String PREF_PRIVATE_PROTOCOL = "private_protocol";
    String PREF_ISPOPUP_ACTIVITY = "ispopup_activity";
    String PREF_NOTIORPOPUP_SETTING = "notiorpopup_setting";
    String PREF_IS_CUST_NOTI = "is_cust_noti";

    String PREF_INBOX_ACTIVITY = "pref_inbox_activity";
    String PREF_PUSH_POPUP_ACTIVITY = "pref_push_popup_activity";
    String PREF_PUSH_POPUP_SHOWING_TIME = "pref_push_popup_showing_time";

    String PREF_NOTI_ICON = "noti_icon";
    String PREF_LARGE_NOTI_ICON = "noti_large_icon";
    String PREF_NOTI_SOUND = "noti_sound";
    String PREF_NOTI_RECEIVER_ACTION = "noti_receiver_action";
    String PREF_PUSH_RECEIVER_ACTION = "push_receiver_action";
    String PREF_NOTI_RECEIVER_CLASS = "noti_receiver_class";
    String PREF_PUSH_RECEIVER_CLASS = "push_receiver_class";
    String PREF_NOTI_RES_ID = "noti_res_id";
    String PREF_NOTI_TITLE_RES_ID = "noti_title_res_id";
    String PREF_NOTI_CONTENT_RES_ID = "noti_content_res_id";

    String PREF_DEVICECERT_STATUS = "pref_devicecert_status";

    String PREF_ONEDAY_LOG = "pref_oneday_log";
    String PREF_YESTERDAY = "pref_yesterday";

    // [start] database
    String DB_UUID = "uuid";
    String DB_NOTI_CHANNEL_ID = "noti_channel_id";

	public static final String DB_NOTIFICATION_CLICK_ACTIVITY_ACTION = "NotificationClickActivityAction";
	public static final String DB_NOTIFICATION_CLICK_ACTIVITY_CLASS = "NotificationClickActivityClass";
	public static final String DB_NOTIFICATION_CLICK_ACTIVITY_FLAGS = "NotificationClickActivityFlags";
	public static final String DB_NOTIFICATION_CLICK_ACTIVITY_FLAGS_ENABLE = "NotificationClickActivityFlagsEnable";
	public static final String DB_NOTIFICATION_CLICK_ACTIVITY_USE_BACKSTACK = "NotificationClickActivityUseBackStack";
    // [end]

    String PREF_FIRST_TIME = "pref_first_time";
    String PREF_SECOND_TIME = "pref_second_time";
    // [end] Preferences

    // [start] Popup Data Key
    String POPUP_ACTIVITY = "popup_activity";
    // Popup Layout Data Key
    String POPUP_BACKGROUND_RES_ID = "popup_background_res_id";
    String POPUP_BACKGROUND_COLOR = "popup_background_color";

    // Top Layout Data Key
    String TOP_LAYOUT_FLAG = "top_layout_flag";
    String TOP_BACKGROUND_RES_ID = "top_background_res_id";
    String TOP_BACKGROUND_COLOR = "top_background_color";
    String TOP_TITLE_TYEP = "top_title_type";
    String TOP_TITLE_TEXT = "text";
    String TOP_TITLE_IMAGE = "image";
    String TOP_TITLE_RES_ID = "top_title_res_id";
    String TOP_TITLE_TEXT_DATA = "top_title_text_data";
    String TOP_TITLE_TEXT_COLOR = "top_text_color";
    String TOP_TITLE_SIZE = "top_text_size";

    // Content Layout Data Key
    String CONTENT_BACKGROUND_RES_ID = "content_background_res_id";
    String CONTENT_BACKGROUND_COLOR = "content_background_color";
    String CONTENT_TEXT_COLOR = "content_text_color";
    String CONTENT_TEXT_SIZE = "content_text_size";

    // Bottom Layout Data Key
    String BOTTOM_TEXT_BTN_COUNT = "bottom_text_btn_count";
    String BOTTOM_RICH_BTN_COUNT = "bottom_rich_btn_count";
    String BOTTOM_BACKGROUND_RES_ID = "bottom_Background_res_id";
    String BOTTOM_BACKGROUND_COLOR = "bottom_Background_color";
    String BOTTOM_BTN_RES_ID = "bottom_btn_res_id";
    String BOTTOM_BTN_TEXT_STATE = "bottom_btn_text_state";
    String BOTTOM_BTN_TEXT_NAME = "bottom_btn_text_name";
    String BOTTOM_BTN_TEXT_COLOR = "bottom_btn_text_color";
    String BOTTOM_BTN_TEXT_SIZE = "bottom_btn_text_size";
    String BOTTOM_BTN_TEXT_CLICKLISTENER = "bottom_btn_text_clickListener";
    String BOTTOM_BTN_RICH_CLICKLISTENER = "bottom_btn_rich_clickListener";

    // XML Layout Data Key
    String XML_LAYOUT_TEXT_RES_ID = "layout_text_res_id";
    String XML_LAYOUT_RICH_RES_ID = "layout_rich_res_id";
    String XML_TEXT_BUTTON_TYPE = "xml_text_button_type";
    String XML_RICH_BUTTON_TYPE = "xml_rich_button_type";
    String XML_TEXT_BUTTON_TAG_NAME = "xml_text_button_tag_name";
    String XML_RICH_BUTTON_TAG_NAME = "xml_rich_button_tag_name";
    String XML_AND_DEFAULT_FLAG = "xml_and_default_flag";

    String PRO_BIG_TEXT_MODE = "big_text_mode";

    int TEXT_DEFAULT_SIZE = 15;
    int TITLE_TEXT_DEFAULT_SIZE = 25;
    int TEXTVIEW = 1;
    int LINEARLAYOUT = 2;
    int WEBVIEW = 3;
    int PROGRESSBAR = 4;
    int IMAGEVIEW = 5;
    int BUTTON_VIEW = 6;
    int IMAGE_BUTTON = 7;
    int RELATIVELAYOUT = 8;
    // [end] Popup Data Key

    // [start] api
    // default encrypt key
    String DEFAULT_ENC_KEY = "Pg-s_E_n_C_k_e_y";
    // time
    long EXPIRE_RETAINED_TIME = 30/* minute */ * 60 * 1000;
    String PREF_LAST_API_TIME = "pref_last_api_time";
    // URL list
    String API_DEVICE_CERT = "deviceCert.m";
    String API_COLLECT_LOG = "collectLog.m";
    String API_LOGIN_PMS = "loginPms.m";
    String API_NEW_MSG = "newMsg.m";
    String API_READ_MSG = "readMsg.m";
    String API_CLICK_MSG = "clickMsg.m";
    String API_SET_CONFIG = "setConfig.m";
    String API_LOGOUT_PMS = "logoutPms.m";
    String API_CHANGEDEVICE_PMS = "changeDevice.m";
    String API_SET_MSGKIND_PMS = "setMsgKind.m";
    String API_DEL_MSG_PMS = "delMsg.m";
    String API_GET_SIGNKEY = "getSignKey.m";
    // pref
    String PREF_APP_USER_ID = "pref_app_user_id";
    String PREF_ENC_KEY = "pref_enc_key";
    String PREF_APP_KEY_CHECK = "pref_app_key_check";
    String PREF_APP_KEY = "pref_app_key";
    String PREF_API_SERVER_CHECK = "pref_api_server_check";
    String PREF_SERVER_URL = "pref_server_url";
    String PREF_MQTT_SERVER_CHECK = "pref_mqtt_server_check";
    String PREF_MQTT_SERVER_SSL_URL = "pref_mqtt_server_ssl_url";
    String PREF_MQTT_SERVER_TCP_URL = "pref_mqtt_server_tcp_url";
    // request params(json) key
    String KEY_API_DEFAULT = "d";
    String KEY_APP_USER_ID = "id";
    String KEY_ENC_PARAM = "data";
    // result params(json) key
    String KEY_API_CODE = "code";
    String KEY_API_MSG = "msg";
    // result code (server)
    String CODE_SUCCESS = "000";
    String CODE_NULL_PARAM = "100";
    String CODE_WRONG_SIZE_PARAM = "101";
    String CODE_WRONG_PARAM = "102";
    String CODE_DECRPYT_FAIL = "103";
    String CODE_PARSING_JSON_ERROR = "104";
    String CODE_WRONG_SESSION = "105";
    String CODE_ENCRYPT_ERROR = "106";
    String CODE_API_INVALID = "109";
    String CODE_INNER_ERROR = "200";
    String CODE_EXTRA_ERROR = "201";
    // result code (client)
    String CODE_SESSION_EXPIRED = "901";
    String CODE_CONNECTION_ERROR = "902";
    String CODE_NOT_RESPONSE = "903";
    String CODE_URL_IS_NULL = "904";
    String CODE_PARAMS_IS_NULL = "905";
    // device cert
    String NO_TOKEN = "noToken";
    // new msg type
    String TYPE_PREV = "P";
    String TYPE_NEXT = "N";

    int SORT_UID_ASC = 1;
    int SORT_UID_DESC = 2;
    int SORT_UID_NO_REGDATE_ASC = 3;
    int SORT_UID_NO_REGDATE_DESC = 4;

    int SORT_SUBSTR_UID_ASC = 1;
    int SORT_SUBSTR_UID_DESC = 2;
    int SORT_FISRT_SUBSTR_UID_ASC = 3;
    int SORT_FISRT_SUBSTR_UID_DESC = 4;
    int SORT_SUBSTR_UID_NO_REGDATE_ASC = 5;
    int SORT_SUBSTR_UID_NO_REGDATE_DESC = 6;
    int SORT_FISRT_SUBSTR_UID_NO_REGDATE_ASC = 7;
    int SORT_FISRT_SUBSTR_UID_NO_REGDATE_DESC = 8;
    // auth status
    String DEVICECERT_PENDING = "devicecert_pending";
    String DEVICECERT_PROGRESS = "devicecert_progress";
    String DEVICECERT_FAIL = "devicecert_fail";
    String DEVICECERT_COMPLETE = "devicecert_complete";
    // [end] api

    String META_DATA_NOTI_O_BADGE ="PMS_NOTI_O_BADGE";

    public static final String DB_CORRECT_USER_MSG_ID = "correct_user_msg_id";

    public static final String PREF_LAUNCHER_BADGE_COUNT = "pref_badge_count";
    public static final String PREF_NOTIFICATION_CHANNEL_NAME = "pref_notification_channel_name";
    public static final String PREF_NOTIFICATION_CHANNEL_DESCRIPTION = "pref_notification_channel_description";
    public static final String PREF_DONOTDISTURB_CHANNEL_NAME = "pref_donotdisturb_channel_name";
    public static final String PREF_DONOTDISTURB_CHANNEL_DESCRIPTION = "pref_donotdisturb_channel_description";
}
