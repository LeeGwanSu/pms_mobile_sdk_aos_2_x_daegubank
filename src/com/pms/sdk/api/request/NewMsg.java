package com.pms.sdk.api.request;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;

public class NewMsg extends BaseRequest {

	private String pType;
	private String pReqUserMsgId;
	private String pMsgGrpCode;
	private String pPageNum;
	private String pPageSize;
	private APICallback pApiCallback;

	private static final String KEY_MAX_ID = "MaxId";
	private static final String KEY_MIN_ID = "MinId";
	private static final String KEY_SENT_USER_MSG_ID = "RmaxUserMsgId";
	private static final String KEY_MSG_CNT = "Total";
	private static final String KEY_SAVED_USER_MSG_ID = "savedReqUserMsgId";

	public NewMsg(Context context) {
		super(context);
	}

	/**
	 * get param
	 *
	 * @return
	 */
	public JSONObject getParam (String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			if(reqUserMsgId.equals("-1"))
			{
				type = "P";
			}
			jobj.put("type", type);
			jobj.put("reqUserMsgId", reqUserMsgId);
			jobj.put("msgGrpCd", msgGrpCode);
			jobj.put("custId", PMSUtil.getCustId(mContext));

			JSONObject page = new JSONObject();
			page.put("page", pageNum);
			page.put("row", pageSize);

			jobj.put("pageInfo", page);

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 *
	 * @param apiCallback
	 */
	public void request (String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize, final APICallback apiCallback) {
		request(type, reqUserMsgId, msgGrpCode, pageNum, pageSize, apiCallback, 0);
	}

	public void request (String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize, final APICallback apiCallback, final int reAttemptCount) {

		isComplete = false;

		pType = type;
		pReqUserMsgId = reqUserMsgId;
		pMsgGrpCode = msgGrpCode;
		pPageNum = pageNum;
		pPageSize = pageSize;
		pApiCallback = apiCallback;

		try {
			apiManager.call(IPMSConsts.API_NEW_MSG, getParam(type, reqUserMsgId, msgGrpCode, pageNum, pageSize), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json, reAttemptCount);
					} else {
						isComplete = true;
					}

					if (apiCallback != null && isComplete) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 *
	 * @param json
	 */
	private boolean requiredResultProc (final JSONObject json, final int reAttemptCount) {
//		CLog.d(" requiredResultProc 재시도 횟수 1  : " + reAttemptCount);
		try {
			// recent msg map
			Map<String, Msg> newMsgList = new HashMap<String, Msg>();

			mPrefs.putString(IPMSConsts.PREF_MAX_USER_MSG_ID, json.getString("maxUserMsgId"));

			// get msg list (json array)
			JSONArray msgList = json.getJSONArray("msgs");
			JSONArray recoveredList = json.getJSONArray("recoveredMsgIds");

			int msgListSize = msgList.length();
			int recoveredMsgIdListSize = recoveredList.length();
			CLog.i("msgListSize=" + msgListSize);
			CLog.i("recoveredMsgIdListSize=" + recoveredMsgIdListSize);
			if (msgListSize < 1 && recoveredMsgIdListSize < 1) {
				isComplete = true;
				return true;
			}

			for (int i = 0; i < msgListSize; i++) {
				Msg msg = new Msg(msgList.getJSONObject(i));
				if (Long.parseLong(msg.expireDate) / 1000000 < Long.parseLong(DateUtil.getNowDate()) / 1000000) {
					continue;
				}

				Msg exMsg = mDB.selectMsgWhereUserMsgId(msg.userMsgId);
				if (exMsg != null) {
					// 2018.07.16 Update iOS 와 통일하기 위해 수행하지 않도록 수정
					mDB.updateMsg(msg);
				} else {
					mDB.insertMsg(msg);
				}

				if ((!Msg.TYPE_H.equals(msg.msgType) || !Msg.TYPE_L.equals(msg.msgType))
						&& (!newMsgList.containsKey(msg.msgGrpCd) || (newMsgList.containsKey(msg.msgGrpCd)
						&& Long.parseLong(newMsgList.get(msg.msgGrpCd).regDate) <= Long.parseLong(msg.regDate) && Long.parseLong(newMsgList
						.get(msg.msgGrpCd).msgId) < Long.parseLong(msg.msgId)))) {
					// rich메시지가 아니면서,
					// 같은 msgCode를 가진 msg가 map에 존재하지 않거나,
					// 같은 msgCode를 가진 msg가 map에 존재하면서, regDate가 같거나 크고, msgId가 큰 경우
					newMsgList.put(msg.msgGrpCd, msg);
				}
			}

			// set msg grp (recent)
			Iterator<String> it = newMsgList.keySet().iterator();
			while (it.hasNext()) {
				String recentMsgCode = it.next();

				// set recent msg
				Msg recentMsg = newMsgList.get(recentMsgCode);
				if (mDB.selectMsgGrp(recentMsgCode) != null) {
					// update msg grp (already exist)
					mDB.updateRecentMsgGrp(recentMsg);
				} else {
					// insert msg grp (no exist)
					mDB.insertMsgGrp(recentMsg);
				}
			}

			// update Recovery Flag
			for (int i = 0; i < recoveredMsgIdListSize; i++) {
				mDB.updateRecoveryFlag(recoveredList.getString(i));
			}

			try {
				if (msgListSize >= Integer.parseInt(pPageSize)) {
					request(pType, pReqUserMsgId, pMsgGrpCode, (Integer.parseInt(pPageNum) + 1) + "", pPageSize, pApiCallback, reAttemptCount);
				} else {
					isComplete = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			mDB.deleteExpireMsg();
			mDB.deleteEmptyMsgGrp();

			// requery
//			mContext.sendBroadcast(new Intent(IPMSConsts.RECEIVER_REQUERY));
		}
	}
}
