package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;

public class LogoutPms extends BaseRequest {

	public LogoutPms(Context context) {
		super(context);
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			// PMSUtil.setEncKey(mContext, "");
			PMSUtil.setCustId(mContext, "");
			mPrefs.putString(PREF_LOGINED_CUST_ID, "");
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
			mDB.deleteAll();

			apiManager.call(API_LOGOUT_PMS, new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
