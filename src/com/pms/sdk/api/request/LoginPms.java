package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.StringUtil;

public class LoginPms extends BaseRequest {

	public LoginPms(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String custId, JSONObject userData) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("custId", custId);

			//15년도 2월에 대구은행 초기 설계에 들어간 기기정보 변경(dupFlag)를 도출하기 위한 커스텀사항이라고 함
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			jobj.put("uuid", PMSUtil.getUUID(mContext));

			if (userData != null) {
				jobj.put("userData", userData);
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param custId
	 * @param userData
	 * @param apiCallback
	 */
	public void request (String custId, final JSONObject userData, final APICallback apiCallback) {
		try {
			mPrefs.putString(PREF_CUST_ID, custId);
			if (!custId.equals(mPrefs.getString(PREF_LOGINED_CUST_ID))) {
				// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
				CLog.i("LoginPms:new user");
				mDB.deleteAll();
				mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
			}

			apiManager.call(API_LOGIN_PMS, getParam(custId, userData), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		String custId = PMSUtil.getCustId(mContext);
		if (!StringUtil.isEmpty(custId)) {
			mPrefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
		}

		try {
			mPrefs.putString(PREF_DUP_FLAG, json.getString("dupflag"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
