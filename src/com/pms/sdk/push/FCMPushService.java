package com.pms.sdk.push;

import android.content.Context;
import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.push.mqtt.MQTTBinder;
import com.pms.sdk.push.mqtt.MQTTService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ydmner on 7/27/16.
 */
public class FCMPushService extends FirebaseMessagingService implements IPMSConsts
{
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            CLog.i("FCM OnMessageReceived From : " + remoteMessage.getFrom());
            CLog.i("Message data payload: " + remoteMessage.getData());

            if (PMSUtil.getGCMProjectId(this).equals(remoteMessage.getFrom()))
            {
                Intent broadcastIntent = new Intent(getApplicationContext(), PushReceiver.class);
                broadcastIntent.setAction(ACTION_RECEIVE);
                broadcastIntent.addCategory(getApplication().getPackageName());

                broadcastIntent.putExtra(MQTTBinder.KEY_MSG, new JSONObject(remoteMessage.getData()).toString());
                broadcastIntent.putExtra("message_id", remoteMessage.getMessageId());
                sendBroadcast(broadcastIntent);
            } else {
                CLog.e("No Match SenderID");
            }
        } catch (Exception e) {
            CLog.e(e.getMessage());
        }
    }

    @Override
    public void onMessageSent(String msgId) {
        CLog.e("onMessageSent() " + msgId);
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String s, Exception e) {
        CLog.e("onSendError() " + s + ", " + e.toString());
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(String token)
    {
        super.onNewToken(token);
        PMSUtil.setGCMToken(this, token);
        CLog.i("onNewToken, registration ID = " + token);
    }

//    public static void upStreamMsg (Context con, String msgid, Intent intent) {
//        CLog.e("upStreamMsg() " + "message_id = " + msgid);
//        PushMsg msg = new PushMsg(intent.getExtras());
//        JSONObject data = new JSONObject();
//        try {
//            data.put(KEY_MSG_ID, msg.msgId);
//            data.put(KEY_NOTI_TITLE, msg.notiTitle);
//            data.put(KEY_NOTI_MSG, msg.notiMsg);
//            data.put(KEY_NOTI_IMG, msg.notiImg);
//            data.put(KEY_MSG, msg.message);
//            data.put(KEY_SOUND, msg.sound);
//            data.put(KEY_MSG_TYPE, msg.msgType);
//            data.put(KEY_DATA, msg.data);
//        } catch (JSONException e) {
//            CLog.e(e.getMessage());
//        }
//
//        FirebaseMessaging fm = FirebaseMessaging.getInstance();
//        fm.send(new RemoteMessage.Builder(PMSUtil.getGCMProjectId(con) + "@gcm.googleapis.com").setMessageId(msgid).setMessageType("upStream")
//                .addData("data", data.toString()).addData("res_date", DateUtil.getNowDate()).setTtl(600).build());
//    }
}
