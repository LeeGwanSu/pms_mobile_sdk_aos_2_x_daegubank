package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTBinder;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.BitmapLruCache;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;


/**
 * push receiver
 *
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

    // notification id
    private final static int NOTIFICATION_ID = 0x253470;
    private static final int START_TASK_TO_FRONT = 2;
    private static final String USE = "Y";
    private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;
    private static final String NOTIFICATION_GROUP = "com.tms.sdk.notification_type";
    private static final int DEFAULT_SHOWING_TIME = 30000;
    private static int sNotificationId = NOTIFICATION_GROUP_SUMMARY_ID + 1;
    private static BigStyleNotification bigStyleNotification = null;
    private static boolean mStackEnable;
    private final Handler mFinishHandler = new Handler();
    private Prefs mPrefs;
    private PowerManager pm;
    private PowerManager.WakeLock wl;

    public static final int BADGE_TYPE_CANCELED = 1;
    public static final int BADGE_TYPE_CLICKED = 2;
    public static final int BADGE_TYPE_UPDATE = 3;
    public static final int BADGE_TYPE_CLEAR = 4;

    public static final String BADGE_ACTION = "pushreceiver.badge.action";
    public static final String BADGE_EXTRA = "pushreceiver.badge.extra";
    public static final String BADGE_TYPE = "pushreceiver.badge.type";
    public static final String BADGE_TYPE_TIME = "pushreceiver.badge.type.time";
    public static final String BADGE_TYPE_COUNT_INTIME = "pushreceiver.badge.update.countInTime";

    public static final int BADGE_BEGINNING_NUMBER = 0;

    private static final int NOTIFICATION_SUMMARY_ID = 30000;
    public static final String NOTIFICATION_CHANNEL_DO_NOT_DISTURB = "notificationChannelDoNotDisturb";

    /**
     * finish runnable
     */
    private final Runnable finishRunnable = new Runnable() {
        @Override
        public void run() {
            if (wl != null && wl.isHeld()) {
                wl.release();
            }
        }
    };
    private Bitmap mPushImage;
    private boolean mPushImageAvailable = false;
    //    private Boolean mbPushCancel = true;
    private NotificationManager mNotificationManager = null;

    public static void initNotification(boolean bStack) {
        if (bigStyleNotification == null) {
            mStackEnable = bStack;
        } else {
            if (mStackEnable != bigStyleNotification.isStackEnable()) {
                mStackEnable = bigStyleNotification.isStackEnable();
                bigStyleNotification.cleanNotification();
            }
        }
    }

    public static boolean isStackNotification(Context context) {
        return FLAG_Y.equals(PMS.getInstance(context).getNotiStackEnable());
    }

    @Override
    public synchronized void onReceive(final Context context, final Intent intent)
    {
        if (intent == null || intent.getAction() == null)
        {
            if (intent != null)
                CLog.e("intent action is null, intent : " + intent.toString());
            return;
        }

        CLog.i("onReceive() -> " + intent.toString());

        mPrefs = new Prefs(context);

        if(!intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE"))
        {
            try
            {
                String message = intent.getStringExtra(MQTTBinder.KEY_MSG);

                JSONObject msgObj = new JSONObject(message);
                if (msgObj.has(KEY_MSG_ID)) {
                    intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
                }
                if (msgObj.has(KEY_NOTI_TITLE)) {
                    intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
                }
                if (msgObj.has(KEY_MSG_TYPE)) {
                    intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
                }
                if (msgObj.has(KEY_NOTI_MSG)) {
                    intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
                }
                if (msgObj.has(KEY_MSG)) {
                    intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
                }
                if (msgObj.has(KEY_SOUND)) {
                    intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
                }
                if (msgObj.has(KEY_NOTI_IMG)) {
                    intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
                }
                if (msgObj.has(KEY_COLOR_FLAG)) {
                    intent.putExtra(KEY_COLOR_FLAG, msgObj.getString(KEY_COLOR_FLAG));
                }
                if (msgObj.has(KEY_TITLE_COLOR)) {
                    intent.putExtra(KEY_TITLE_COLOR, msgObj.getString(KEY_TITLE_COLOR));
                }
                if (msgObj.has(KEY_CONTENT_COLOR)) {
                    intent.putExtra(KEY_CONTENT_COLOR, msgObj.getString(KEY_CONTENT_COLOR));
                }
                if (msgObj.has(KEY_NOT_POPUP)) {
                    intent.putExtra(KEY_NOT_POPUP, msgObj.getString(KEY_NOT_POPUP));
                }
                if (msgObj.has(KEY_DATA)) {
                    intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
                }
            }
            catch (Exception e)
            {
                CLog.e(e.toString());
            }

            if (BADGE_ACTION.equals(intent.getAction()))
            {
                int type = intent.getIntExtra(BADGE_TYPE, 0);
                switch (type)
                {
                    case BADGE_TYPE_CLICKED:
                        sendClickEvent(context, intent.getExtras());
                        decreaseLauncherBadge(context);
                        break;
                    case BADGE_TYPE_CANCELED:
                        long badgeUpdateTime = mPrefs.getLong(BADGE_TYPE_TIME);
                        long currentTime = new Date().getTime();
                        int badgeUpdateCountInTime = mPrefs.getInt(BADGE_TYPE_COUNT_INTIME);
                        if(badgeUpdateTime == 0)
                        {
                            mPrefs.putLong(BADGE_TYPE_TIME, currentTime);
                            mPrefs.putInt(BADGE_TYPE_COUNT_INTIME, ++badgeUpdateCountInTime);
                            decreaseLauncherBadge(context);
                        }
                        else
                        {
                            if((currentTime - badgeUpdateTime) < 5000)
                            {
                                if(badgeUpdateCountInTime > 40)
                                {
                                    clearLauncherBadge(context, false);
                                    mPrefs.putLong(BADGE_TYPE_TIME, currentTime);
                                    mPrefs.putInt(BADGE_TYPE_COUNT_INTIME, 0);
                                }
                                else
                                {
                                    decreaseLauncherBadge(context);
                                    mPrefs.putInt(BADGE_TYPE_COUNT_INTIME, ++badgeUpdateCountInTime);
                                }
                            }
                            else
                            {
                                decreaseLauncherBadge(context);
                                mPrefs.putLong(BADGE_TYPE_TIME, currentTime);
                                mPrefs.putInt(BADGE_TYPE_COUNT_INTIME, 0);
                            }
                        }
                        break;
                    case BADGE_TYPE_UPDATE:
                        int value = intent.getIntExtra(BADGE_EXTRA, 0);
                        updateLauncherBadge(context, value);
                        break;
                    case BADGE_TYPE_CLEAR:
                        boolean isClearNotification = intent.getBooleanExtra(BADGE_EXTRA, false);
                        clearLauncherBadge(context, isClearNotification);
                        break;
                }
            }
            else
            {
                // receive push message
                if (MQTTBinder.ACTION_RECEIVED_MSG.equals(intent.getAction()))
                {
                    // private server
                    CLog.i("onReceive:receive from private server");

                }
                else if (ACTION_RECEIVE.equals(intent.getAction()))
                {
                    // gcm
                    CLog.i("onReceive:receive from FCM(GCM)");
                }

                onPushMessage(context, intent);
            }
        }
    }

    private boolean isDisturb;
    private synchronized void onPushMessage(final Context context, final Intent intent) {
        String firstTime = PMSUtil.getFirstTime(context);
        String secondTime = PMSUtil.getSecondTime(context);

        CLog.i("Allowed Time Flag : " + PMSUtil.getAllowedTime(context));

        isDisturb = false;
        if (PMSUtil.getAllowedTime(context))
        {
            if (!TextUtils.isEmpty(firstTime) && !TextUtils.isEmpty(secondTime))
            {
                if (isCheckTime(firstTime, secondTime))
                {
                    CLog.i("The current time is the time when the notification stopped.");
                    isDisturb = true;
                }
            }
        }

        if (isImagePush(intent.getExtras())) {
            // image push
            QueueManager queueManager = QueueManager.getInstance();
            RequestQueue queue = queueManager.getRequestQueue();
            queue.getCache().clear();
            ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
            imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
                @Override
                public void onResponse(ImageContainer response, boolean isImmediate) {
                    if (response == null) {
                        CLog.e("response is null");
                        return;
                    }
                    if (response.getBitmap() == null) {
                        CLog.e("bitmap is null");
                        return;
                    }
                    mPushImage = response.getBitmap();
                    mPushImageAvailable = true;
                    CLog.i("imageWidth:" + mPushImage.getWidth());
                    onMessage(context, intent);
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    CLog.e("onErrorResponse:" + error.getMessage());
                    // wrong img url (or exception)
                    mPushImageAvailable = false;
                    onMessage(context, intent);
                }
            });

        } else {
            // default push
            onMessage(context, intent);
        }
    }

    /**
     * on message (gcm, private msg receiver)
     *
     * @param context
     * @param intent
     */
    @SuppressWarnings("deprecation")
    private synchronized void onMessage(final Context context, Intent intent) {
        CLog.i("onMessage() isDisturb "+ isDisturb);
        final Bundle extras = intent.getExtras();

        PMS pms = PMS.getInstance(context);

        PushMsg pushMsg = new PushMsg(extras);
        CLog.i(pushMsg + "");

        if (!isDisturb)
        {
            if (FLAG_Y.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || !StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG)))
            {
                // screen on
                pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                if (!pm.isScreenOn())
                {
                    wl.acquire();
                    mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
                }
            }
        }

        if (StringUtil.isEmpty(pushMsg.msgId) || StringUtil.isEmpty(pushMsg.notiTitle) || StringUtil.isEmpty(pushMsg.notiMsg) || StringUtil.isEmpty(pushMsg.msgType))
        {
            CLog.i("msgId or notiTitle or notiMsg or msgType is null");

            String privateFlag = PMSUtil.getPrivateFlag(context);
            String mktFlag = PMSUtil.getMQTTFlag(context);
            CLog.d("privateFlag: " + privateFlag + ", mktFlag: " + mktFlag);

            if(FLAG_Y.equals(privateFlag) && FLAG_Y.equals(mktFlag))
            {
                Intent i = new Intent(context, RestartReceiver.class);
                i.setAction(MQTTService.ACTION_FORCE_START);
                context.sendBroadcast(i);
            }

            return;
        }

        PMSDB db = PMSDB.getInstance(context);

        // check already exist msg
        Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
        if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
            CLog.i("already exist msg");
            return;
        }

        // insert (temp) new msg
        Msg newMsg = new Msg();
        newMsg.readYn = Msg.FLAG_N;
        newMsg.msgGrpCd = "999999";
        newMsg.expireDate = "0";
        newMsg.msgId = pushMsg.msgId;

        db.insertMsg(newMsg);

        // refresh list and badge
        Intent intentPush = null;
        String receiverClass = mPrefs.getString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS);
        String receiverAction = mPrefs.getString(IPMSConsts.PREF_PUSH_RECEIVER_ACTION);
        if(TextUtils.isEmpty(receiverAction))
        {
            receiverAction = "com.pms.sdk.event."+context.getPackageName();
        }
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                intentPush = new Intent(context, cls).putExtras(extras);
                intentPush.setAction(receiverAction);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }
        if (intentPush == null) {
            intentPush = new Intent(receiverAction).putExtras(extras);
        }

        if (intentPush != null) {
            intentPush.addCategory(context.getPackageName());
            context.sendBroadcast(intentPush);
        }

        // show noti
        // ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
        String notiFlag =  mPrefs.getString(PREF_NOTI_FLAG);
        CLog.i("NOTI FLAG : " + notiFlag);

        if (USE.equals(notiFlag) || StringUtil.isEmpty(notiFlag)) {
            // check push flag

            CLog.i("version code :" + Build.VERSION.SDK_INT);

            // execute push noti listener
            if (pms.getOnReceivePushListener() != null) {
                CLog.i("ReceivePushListener is not null");
                if (pms.getOnReceivePushListener().onReceive(context, extras)) {
                    showNotification(context, extras);
                }
            } else {
                CLog.i("ReceivePushListener is null");
                showNotification(context, extras);
            }

            CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));
            CLog.i("NOT POPUP FLAG : " + pushMsg.notPopup);

            if ((USE.equals(pushMsg.notPopup)) && (USE.equals(mPrefs.getString(PREF_ALERT_FLAG))))
            {
                showPopup(context, extras);
            }
        }
    }

    /**
     * show notification
     *
     * @param context
     * @param extras
     */
    private synchronized void showNotification(Context context, Bundle extras) {
        CLog.i("showNotification");
        if (mPushImageAvailable) {
            showNotificationImageStyle(context, extras);
        } else {
            showNotificationTextStyle(context, extras);
        }
    }

    /**
     * show notification text style
     *
     * @param context
     * @param extras
     */
    @SuppressLint("NewApi")
    private synchronized void showNotificationTextStyle(Context context, Bundle extras) {
        CLog.i("showNotificationTextStyle");
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel))
        {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1)
        {
            if(isDisturb)
            {
                strNotiChannel = createDoNotDisturbNotiChannel(context,notificationManager);
            }
            else
            {
                strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            }
            builder = new NotificationCompat.Builder(context, strNotiChannel);
        }
        else {
            builder = new NotificationCompat.Builder(context);
        }
        builder.setNumber(1);

        if (isDisturb)
        {
            builder.setVibrate(new long[]{0});
            builder.setSound(null);
            builder.setPriority(Notification.PRIORITY_LOW);
        }
        else
        {
            builder.setDefaults(Notification.DEFAULT_ALL);
            builder.setPriority(Notification.PRIORITY_MAX);

            // 진동
            if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG)))
            {
                builder.setVibrate(new long[]{0, 1000});
            }

            final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

            if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
                if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG)))
                {
                    try
                    {
                        int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                        CLog.d("notiSound : "+notiSound);
                        if (notiSound > 0) {
                            Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                            builder.setSound(uri);
                        } else {
                            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            builder.setSound(uri);
                        }
                    }
                    catch (Exception e)
                    {
                        CLog.e(e.getMessage());
                    }
                }
                else
                {
                    CLog.d("ring flag N");
                }
            }
        }
        int newNotificationId = getNewNotificationId();
        builder.setContentIntent(makePendingIntent(context, extras, BADGE_TYPE_CLICKED, newNotificationId));
        builder.setDeleteIntent(clickListenerPendingIntent(context, extras, BADGE_TYPE_CANCELED, newNotificationId));
        builder.setAutoCancel(true);
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        builder.setGroup(NOTIFICATION_GROUP);

        // setting lights color
        builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);
        int iconId = 0;
        int largeIconId = 0;
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.LOLLIPOP) {
            iconId = PMSUtil.getIconId(context);
            largeIconId = mPrefs.getInt(PREF_LARGE_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_LARGE_NOTI_ICON) : 0;
        } else {
            iconId = PMSUtil.getIconId(context);
        }

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);
        if (largeIconId > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
        }

        int SDK_INT = Build.VERSION.SDK_INT;
        if (PMSUtil.getIsCustNoti(context) && (FLAG_Y.equals(pushMsg.colorFlag)) && (SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)) {
            RemoteViews cutmNoti = new RemoteViews(context.getApplicationContext().getPackageName(), PMSUtil.getNotiResId(context));
            builder.setContent(cutmNoti);
            cutmNoti.setTextViewText(PMSUtil.getNotiTitleResId(context), pushMsg.notiTitle);
            cutmNoti.setTextColor(PMSUtil.getNotiTitleResId(context), Color.parseColor(pushMsg.titleColor));
            cutmNoti.setTextViewText(PMSUtil.getNotiContenResId(context), pushMsg.notiMsg);
            cutmNoti.setTextColor(PMSUtil.getNotiContenResId(context), Color.parseColor(pushMsg.contentColor));
        }

        if (mNotificationManager == null)
            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // 초기화 기존 notification clean
        boolean stackEnable = isStackNotification(context);
//        initNotification(stackEnable);

        // show notification
        bigStyleNotification = new BigStyleNotification(context, pushMsg, mNotificationManager, builder, stackEnable, false).invoke();
        bigStyleNotification.showNotification();
    }

    /**
     * show notification image style
     *
     * @param context
     * @param extras
     */
    @SuppressLint("NewApi")
    private synchronized void showNotificationImageStyle(Context context, Bundle extras) {
        CLog.i("showNotificationImageStyle");
        // push info
        PushMsg pushMsg = new PushMsg(extras);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel)) {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1)
        {
            if(isDisturb)
            {
                strNotiChannel = createDoNotDisturbNotiChannel(context,notificationManager);
            }
            else
            {
                strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            }
            builder = new NotificationCompat.Builder(context, strNotiChannel);
        }
        else {
            builder = new NotificationCompat.Builder(context);
        }
        builder.setNumber(1);

        if (isDisturb)
        {
            builder.setPriority(Notification.PRIORITY_LOW);
            builder.setVibrate(new long[]{0});
            builder.setSound(null);
        }
        else
        {
            builder.setPriority(Notification.PRIORITY_MAX);
            // 진동
            if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG)))
            {
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
            }

            final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

            if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
                if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG)))
                {
                    try
                    {
                        int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                        CLog.d("notiSound : "+notiSound);
                        if (notiSound > 0) {
                            Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                            builder.setSound(uri);
                        } else {
                            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            builder.setSound(uri);
                        }
                    } catch (Exception e) {
                        CLog.e(e.getMessage());
                    }
                }
                else
                {
                    CLog.d("ring flag N");
                }
            }
        }
        builder.setContentTitle(pushMsg.notiTitle);

        int newNotificationId = getNewNotificationId();
        builder.setContentIntent(makePendingIntent(context, extras, BADGE_TYPE_CLICKED, newNotificationId));
        builder.setDeleteIntent(clickListenerPendingIntent(context, extras, BADGE_TYPE_CANCELED, newNotificationId));
        builder.setAutoCancel(true);
        builder.setContentText(StringUtil.isEmpty(PMSUtil.getBigNotiContextMsg(context)) ? pushMsg.notiMsg : PMSUtil.getBigNotiContextMsg(context));
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(0xFF00FF00, 1000, 2000);
        builder.setGroup(NOTIFICATION_GROUP);

        // set small icon
        int iconId = 0;
        int largeIconId = 0;
        int ver = Build.VERSION.SDK_INT;
        if (ver >= Build.VERSION_CODES.LOLLIPOP) {
            iconId = PMSUtil.getIconId(context);
            largeIconId = mPrefs.getInt(PREF_LARGE_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_LARGE_NOTI_ICON) : 0;
        } else {
            iconId = PMSUtil.getIconId(context);
        }

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);
        if (largeIconId > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
        }

        if (mPushImage == null) {
            CLog.e("mPushImage is null");
        }

        if (mNotificationManager == null)
            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        boolean stackEnable = isStackNotification(context);

        bigStyleNotification = new BigStyleNotification(context, pushMsg, mNotificationManager, builder, mPushImage, stackEnable, false).invoke();
        bigStyleNotification.showNotification();
    }

    private PendingIntent makePendingIntent (Context context, Bundle extras, int type, int requestCode) {
	    String activityAction = PMSUtil.getNotificationClickActivityAction(context);
	    String activityClass = PMSUtil.getNotificationClickActivityClass(context);
	    if(TextUtils.isEmpty(activityClass)){
		    return clickListenerPendingIntent(context, extras, type, requestCode);
	    }

	    Intent intent = null;
	    try	{
		    Class<?> cls = Class.forName(activityClass);
		    intent = new Intent(context, cls);
	    } catch (ClassNotFoundException e) {
		    CLog.i("cannot found ("+(activityClass)+")");
	    }

	    if(intent == null){
		    return null;
	    }

	    intent.putExtras(extras);
	    intent.setAction(activityAction);

	    if(PMSUtil.getNotificationClickActivityUseBackStack(context)) {
		    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		    stackBuilder.addNextIntentWithParentStack(intent);
		    if (PMSUtil.getTargetVersion(context) >= 31) {
			    return stackBuilder.getPendingIntent(requestCode + type, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		    } else {
			    return stackBuilder.getPendingIntent(requestCode + type, PendingIntent.FLAG_UPDATE_CURRENT);
		    }
	    } else {
		    if(PMSUtil.getNotificationClickActivityFlagEnable(context)) {
			    try {
				    if(PMSUtil.getNotificationClickActivityFlag(context) != null) {
					    intent.setFlags(Integer.parseInt(PMSUtil.getNotificationClickActivityFlag(context)));
				    }
			    } catch (NumberFormatException numberFormatException) {
				    numberFormatException.printStackTrace();
			    }
		    }
		    if (PMSUtil.getTargetVersion(context) >= 31) {
			    return PendingIntent.getActivity(context, requestCode + type, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		    } else {
			    return PendingIntent.getActivity(context, requestCode + type, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		    }
	    }
    }

	private PendingIntent clickListenerPendingIntent(Context context, Bundle extras, int type, int requestCode) {
		Intent intent = new Intent(context, PushReceiver.class);
		intent.setAction(BADGE_ACTION);
		intent.putExtras(extras);
		intent.putExtra(BADGE_TYPE, type);

		if (PMSUtil.getTargetVersion(context) >= 31) {
			return PendingIntent.getBroadcast(context, requestCode + type, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		} else {
			return PendingIntent.getBroadcast(context, requestCode + type, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		}
	}

    /**
     * show popup (activity)
     *
     * @param context
     * @param extras
     */
    @SuppressLint("DefaultLocale")
    private synchronized void showPopup(Context context, Bundle extras) {
        try {
            PushMsg pushMsg = new PushMsg(extras);

            if (PMSUtil.getNotiOrPopup(context) && pushMsg.msgType.equals("T")) {
                Toast.makeText(context, pushMsg.notiMsg, Toast.LENGTH_SHORT).show();
            } else {
                Class<?> pushPopupActivity = null;
                String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

                if (StringUtil.isEmpty(pushPopupActivityName)) {
                    pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
                }

                try {
                    pushPopupActivity = Class.forName(pushPopupActivityName);
                } catch (ClassNotFoundException e) {
                    CLog.e(e.getMessage());
                    pushPopupActivity = PushPopupActivity.class;
                }

                CLog.i("pushPopupActivity :" + pushPopupActivityName);

                Intent pushIntent = new Intent(context, pushPopupActivity);
                pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                pushIntent.putExtras(extras);

                if (isOtherApp(context)) {
                    context.startActivity(pushIntent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"deprecation", "static-access"})
    @SuppressLint("DefaultLocale")
    private boolean isOtherApp(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        String topActivity = "";

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            RunningAppProcessInfo currentInfo = null;
            Field field = null;
            try {
                field = RunningAppProcessInfo.class.getDeclaredField("processState");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
            for (RunningAppProcessInfo app : appList) {
                if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Integer state = null;
                    try {
                        state = field.getInt(app);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    if (state != null && state == START_TASK_TO_FRONT) {
                        currentInfo = app;
                        break;
                    }
                }
            }
            topActivity = currentInfo.processName;
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
            topActivity = taskInfo.get(0).topActivity.getPackageName();
        }

        CLog.e("TOP Activity : " + topActivity);
        if (topActivity.equals(context.getPackageName())) {
            return true;
        }
        return false;
    }

    /**
     * is image push
     *
     * @param extras
     * @return
     */
    private boolean isImagePush(Bundle extras) {
        try {
            if (!PhoneState.orMoreJellyBean()) {
                throw new Exception("wrong os version");
            }
            String notiImg = extras.getString(KEY_NOTI_IMG);
            CLog.i("notiImg:" + notiImg);
            if (notiImg == null || "".equals(notiImg)) {
                throw new Exception("no image type");
            }
            return true;
        } catch (Exception e) {
            CLog.e("isImagePush:" + e.getMessage());
            return false;
        }
    }

    private boolean isCheckTime(String start, String end) {
        Date currentDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("HHmm");
        return DateUtil.isDoNotDisturb(start, end, format.format(currentDate));
    }

    // BigStyle Notification을 위한 클래스
    private class BigStyleNotification {
        private final Context context;
        private final PushMsg pushMsg;
        private final NotificationCompat.Builder builder;
        private final Bitmap pushImg;
        private boolean summaryGroup = false;
        private Notification notification;
        private NotificationManager notificationManager;

        private boolean stackEnable = false;

        /*******************************************
         * Text Style, (Image가 null일 경우)
         *******************************************/
        public BigStyleNotification(Context context, PushMsg pushMsg, NotificationManager notificationManager, NotificationCompat.Builder builder) {
            this(context, pushMsg, notificationManager, builder, null, false, false);
        }

        public BigStyleNotification(Context context, PushMsg pushMsg, NotificationManager notificationManager, NotificationCompat.Builder builder, boolean stackEnable, boolean summaryGroup) {
            this(context, pushMsg, notificationManager, builder, null, stackEnable, summaryGroup);
        }

        /*******************************************
         * Image Style
         *******************************************/
        public BigStyleNotification(Context context, PushMsg pushMsg, NotificationManager notificationManager, NotificationCompat.Builder builder, Bitmap pushImg) {
            this(context, pushMsg, notificationManager, builder, pushImg, false, false);
        }

        public BigStyleNotification(Context context, PushMsg pushMsg, NotificationManager notificationManager, NotificationCompat.Builder builder, Bitmap pushImg, boolean stackEnable, boolean summaryGroup) {
            this.context = context;
            this.pushMsg = pushMsg;
            this.builder = builder;
            this.pushImg = pushImg;
            this.notificationManager = notificationManager;
            this.stackEnable = stackEnable;
            this.summaryGroup = summaryGroup;
        }

        public Notification getNotification() {
            return notification;
        }

        public void cleanNotification() {
            notificationManager.cancelAll();
        }

        public void showNotification()
        {
            int newNotificationId = getNewNotificationId();
            CLog.i("showNotification(stackEnable) : " + stackEnable + ", newNotificationId: " + newNotificationId);
            if (!stackEnable)
            {
//                bigStyleNotification.cleanNotification();
                clearLauncherBadge(context, true);
            }
            stackShowNotification(newNotificationId);

            /**
             * Android 6.0 아래의 기기는 getActiveNotifications() 메소드를 사용할 수 없으므로 직접 계산
             */
            if(Build.VERSION.SDK_INT < 23)
            {
                increaseLauncherBadge(context);
            }
            else
            {
                increaseLauncherBadge(context);
                if(Build.VERSION.SDK_INT >= 29)
                {
                    if(!isDisturb)
                    {
                        updateNotificationSummary(context);
                    }
                }
                else
                {
                    updateNotificationSummary(context);
                }
            }
        }

        private void stackShowNotification(int newNotificationId) {
            notificationManager.notify(newNotificationId, notification);
        }

        private void updateShowNotification(int newNotificationId) {
            if (PhoneState.orMoreMarshmallow() && summaryGroup) {
                builder.setGroup(NOTIFICATION_GROUP);
                notificationManager.notify(PMS_COMPANY, newNotificationId, notification);
                // [END create_notification]
                CLog.i("Add a notification");
//                updateNotificationSummary(context, notificationManager);
            } else {
                notificationManager.notify(PMS_COMPANY, NOTIFICATION_ID, notification);
            }
        }

        public BigStyleNotification invoke() {
            if (pushImg == null) {
//              notificationText
                if (PhoneState.orMoreJellyBean()) {
                    // gesture (two finger drop down)
                    builder.setContentText(StringUtil.isEmpty(PMSUtil.getBigNotiContextMsg(context)) ? pushMsg.notiMsg : PMSUtil
                            .getBigNotiContextMsg(context));
                    notification = new NotificationCompat.BigTextStyle(builder).bigText(pushMsg.notiMsg).build();
                } else {
                    builder.setContentText(pushMsg.notiMsg);
                    notification = builder.build();
                }
            } else {
//              notificationImg , default gesture(two finger drop down)
                builder.setContentText(StringUtil.isEmpty(PMSUtil.getBigNotiContextMsg(context)) ? pushMsg.notiMsg : PMSUtil
                        .getBigNotiContextMsg(context));
                notification = new NotificationCompat.BigPictureStyle(builder).bigPicture(pushImg).setBigContentTitle(pushMsg.notiTitle).setSummaryText(pushMsg.notiMsg).build();
            }
            return this;
        }

        /**
         * Retrieves a unique notification ID.
         */
        public int getNewNotificationId() {
            int notificationId = (int)(System.currentTimeMillis() % Integer.MAX_VALUE);
            return notificationId;
        }

        public boolean isStackEnable() {
            return stackEnable;
        }


    }

    @TargetApi(Build.VERSION_CODES.O)
    private String createNotiChannel(Context context, NotificationManager notificationManager, String channelId)
    {
        NotificationChannel notiChannel = notificationManager.getNotificationChannel(channelId);
        boolean isShowBadge = false;
        boolean isPlaySound;
        boolean isPlaySoundChanged = false;
        boolean isEnableVibe;
        boolean isShowBadgeOnChannel;
        boolean isPlaySoundOnChannel;
        boolean isEnableVibeOnChannel;

        try
        {
            if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
            {
                isShowBadge = true;
            }
            else
            {
                isShowBadge = false;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)))
        {
            isPlaySound = true;
        }
        else
        {
            isPlaySound = false;
        }
        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)))
        {
            isEnableVibe = true;
        }
        else
        {
            isEnableVibe = false;
        }
        try
        {
            CLog.d("AppSetting isShowBadge "+ context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)+
                    " isPlaySound "+mPrefs.getString(PREF_RING_FLAG)+
                    " isEnableVibe "+mPrefs.getString(PREF_VIBE_FLAG));
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        if (notiChannel == null)
        {    //if notichannel is not initialized
            CLog.d("notification channel is initialized / " + channelId);
            notiChannel = new NotificationChannel(channelId, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setShowBadge(isShowBadge);
            notiChannel.enableLights(true);
            notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
            notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setBypassDnd(true);

            notiChannel.enableVibration(isEnableVibe);
            if (isEnableVibe)
            {
                notiChannel.setVibrationPattern(new long[]{0, 1000});
            }

            if (isPlaySound)
            {
                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                try
                {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    if (notiSound > 0)
                    {
                        uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        CLog.d("notiSound " + notiSound + " uri " + uri.toString());
                    } else
                    {
                        throw new Exception("default ringtone is set");
                    }
                }
                catch (Exception e)
                {
                    uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    CLog.e(e.getMessage());
                }

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                notiChannel.setSound(uri, audioAttributes);
                CLog.d("setChannelSound ring with initialize notichannel");
            }
            else
            {
                notiChannel.setSound(null, null);
            }
        }
        else
        {
            CLog.d("notification channel is exist " + notiChannel);
        }

        String name = mPrefs.getString(IPMSConsts.PREF_NOTIFICATION_CHANNEL_NAME);
        if(TextUtils.isEmpty(name))
        {
            name = PMSUtil.getApplicationName(context);
        }
        notiChannel.setName(name);
        String description = mPrefs.getString(IPMSConsts.PREF_NOTIFICATION_CHANNEL_DESCRIPTION);
        if(!TextUtils.isEmpty(description))
        {
            notiChannel.setDescription(description);
        }

        notificationManager.createNotificationChannel(notiChannel);
        DataKeyUtil.setDBKey(context, DB_NOTI_CHANNEL_ID, channelId);
        return channelId;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private String createDoNotDisturbNotiChannel(Context context, NotificationManager notificationManager)
    {
        boolean isShowBadge = false;
        try
        {
            if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
            {
                isShowBadge = true;
            }
            else
            {
                isShowBadge = false;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        NotificationChannel notiChannel = notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_DO_NOT_DISTURB);

        if (notiChannel == null)
        {
            CLog.d("notification channel is initialized / " + NOTIFICATION_CHANNEL_DO_NOT_DISTURB);

            notiChannel = new NotificationChannel(NOTIFICATION_CHANNEL_DO_NOT_DISTURB, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_LOW);
            notiChannel.setShowBadge(isShowBadge);
            notiChannel.enableVibration(false);
            notiChannel.setVibrationPattern(new long[]{0});
            notiChannel.setBypassDnd(false);
            notiChannel.setSound(null, null);
        }
        else
        {
            CLog.d("notification channel is exist / "+notiChannel);
        }
        String name = mPrefs.getString(IPMSConsts.PREF_DONOTDISTURB_CHANNEL_NAME);
        if(TextUtils.isEmpty(name))
        {
            name = PMSUtil.getApplicationName(context);
        }
        notiChannel.setName(name);
        String description = mPrefs.getString(IPMSConsts.PREF_DONOTDISTURB_CHANNEL_DESCRIPTION);
        if(!TextUtils.isEmpty(description))
        {
            notiChannel.setDescription(description);
        }
        notificationManager.createNotificationChannel(notiChannel);
        return NOTIFICATION_CHANNEL_DO_NOT_DISTURB;
    }

    public int getNewNotificationId () {
        int notificationId = (int)(System.currentTimeMillis() % Integer.MAX_VALUE);
        return notificationId;
    }

    public static int getNotificationId () {
        return sNotificationId;
    }

    /**
     * Adds/updates/removes the notification summary as necessary.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void updateNotificationSummary (Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        final StatusBarNotification[] activeNotifications = manager.getActiveNotifications();

        int numberOfNotifications = activeNotifications.length;
        // Since the notifications might include a summary notification remove it from the count if
        // it is present.
//        int badgeCount = 0;
        for (StatusBarNotification notification : activeNotifications) {
//			if (notification.getId() == NOTIFICATION_SUMMARY_ID) {
//				numberOfNotifications--;
//			}
//            badgeCount += notification.getNotification().number;
        }
//        mPrefs.putInt(IPMSConsts.PREF_LAUNCHER_BADGE_COUNT, badgeCount);
//        CLog.i("updateNotificationSummary sum of notification numbers(badge) : "+badgeCount);
//        updateLauncherBadge(context, badgeCount);

        if(FLAG_Y.equals(PMS.getInstance(context).getNotiStackEnable()))
        {
            if (numberOfNotifications > 1)
            {
                NotificationCompat.Builder builder;

                String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
                if (StringUtil.isEmpty(strNotiChannel))
                {
                    strNotiChannel = "0";
                }

                // Notification channel added
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1)
                {
                    if(isDisturb)
                    {
                        strNotiChannel = createDoNotDisturbNotiChannel(context,manager);
                    }
                    else
                    {
                        strNotiChannel = createNotiChannel(context, manager, strNotiChannel);
                    }
                    builder = new NotificationCompat.Builder(context, strNotiChannel);
                }
                else {
                    builder = new NotificationCompat.Builder(context);
                }

                builder.setPriority(Notification.PRIORITY_LOW)
                        .setContentTitle(PMSUtil.getApplicationName(context))
                        .setContentText("새로운 메시지가 있습니다")
                        .setSound(null)
                        .setGroup(NOTIFICATION_GROUP)
                        .setGroupSummary(true);
                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                for (StatusBarNotification notification : activeNotifications) {
                    if (notification.getId() != NOTIFICATION_SUMMARY_ID) {
                        inboxStyle.addLine(notification.getNotification().tickerText);
                    }
                }
                builder.setStyle(inboxStyle);

                int iconId = 0;
                int largeIconId = 0;
                int ver = Build.VERSION.SDK_INT;
                if (ver >= Build.VERSION_CODES.LOLLIPOP) {
                    iconId = PMSUtil.getIconId(context);
                    largeIconId = PMSUtil.getLargeIconId(context);
                } else {
                    iconId = PMSUtil.getLargeIconId(context);
                }

                // set small icon
                CLog.i("small icon :" + iconId);
                if (iconId > 0) {
                    builder.setSmallIcon(iconId);
                }

                // set large icon
                CLog.i("large icon :" + largeIconId);
                if (largeIconId > 0) {
                    builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
                }


                manager.notify(NOTIFICATION_SUMMARY_ID, builder.build());
            } else {
                // Remove the notification summary.
                manager.cancel(NOTIFICATION_SUMMARY_ID);
            }
        }
    }

    private void sendClickEvent(Context context, Bundle extras)
    {
        Intent innerIntent = null;
        String receiverAction = null;
        String receiverClass = null;
        receiverAction = mPrefs.getString(IPMSConsts.PREF_NOTI_RECEIVER_ACTION);
        if(TextUtils.isEmpty(receiverAction))
        {
            receiverAction = "com.pms.sdk.click."+context.getPackageName();
        }
        receiverClass = mPrefs.getString(IPMSConsts.PREF_NOTI_RECEIVER_CLASS);

        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                innerIntent = new Intent(context, cls).putExtras(extras);
                innerIntent.setAction(receiverAction);

            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }

        if (innerIntent == null) {
            CLog.d("innerIntent == null");
            // setting push info to intent
            innerIntent = new Intent(receiverAction).putExtras(extras);
        }

        context.sendBroadcast(innerIntent);
    }

    private void increaseLauncherBadge(Context context)
    {
        int recentBadgeCount = mPrefs.getInt(IPMSConsts.PREF_LAUNCHER_BADGE_COUNT, BADGE_BEGINNING_NUMBER);
        recentBadgeCount += 1;

        updateLauncherBadge(context, recentBadgeCount);
    }

    private void decreaseLauncherBadge(Context context)
    {
        int recentBadgeCount = mPrefs.getInt(IPMSConsts.PREF_LAUNCHER_BADGE_COUNT, BADGE_BEGINNING_NUMBER);
        if(recentBadgeCount > BADGE_BEGINNING_NUMBER)
        {
            recentBadgeCount -= 1;
        }
        else
        {
            recentBadgeCount = BADGE_BEGINNING_NUMBER;
        }

        updateLauncherBadge(context, recentBadgeCount);
    }

    private void updateLauncherBadge(Context context, int count)
    {

        if(Build.VERSION.SDK_INT < 26)
        {
            CLog.d("updateLauncherBadge : " + count);
            mPrefs.putInt(IPMSConsts.PREF_LAUNCHER_BADGE_COUNT, count);

            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            if(launchIntent != null)
            {
                ComponentName componentName = launchIntent.getComponent();
                Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
                intent.putExtra("badge_count_package_name", componentName.getPackageName());
                intent.putExtra("badge_count_class_name", componentName.getClassName());
                intent.putExtra("badge_count", count);

//            CLog.d("badge count : " + count + " package : " + componentName.getPackageName()+" class : " + componentName.getClassName());
                context.sendBroadcast(intent);
            }
        }
    }

    private void clearLauncherBadge(Context context, boolean clearNotification)
    {
        if(clearNotification)
        {
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancelAll();
        }
        updateLauncherBadge(context, 0);
    }
}