package com.pms.sdk.common.util;

import android.content.Context;

import com.pms.sdk.IPMSConsts;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProPertiesFileUtil implements IPMSConsts {

	private static Properties mProperty = null;

	private static void propertiesFileOpen (Context con) {
		try {
			mProperty = new Properties();
			InputStream is = con.getAssets().open(PMS_PROPERY_NAME);
			mProperty.load(is);
			is.close();
		} catch (IOException e) {
//			CLog.e(e.getMessage());
		} catch (Exception e)
		{
			CLog.w(e.getMessage());
		}
	}

	public static String getString (Context con, String key) {
		propertiesFileOpen(con);
		String value = mProperty.getProperty(key);
		if (StringUtil.isEmpty(value)) {
			return "";
		} else {
			return value;
		}
	}
}
