package com.pms.sdk.common.util;

import java.util.regex.Pattern;

/**
 * @since 2012.01.14
 * @author erzisk
 * @description string util
 */
public class StringUtil {

	/**
	 * is empty arr (or)
	 * 
	 * @param arrStr
	 * @return true : array이 안의 string중 하나라도 "" or null 이면 true
	 */
	public static boolean isEmptyArr (String[] arrStr) {
		for (String s : arrStr) {
			if (isEmpty(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * is empty
	 * 
	 * @param str
	 * @return true : "" or null
	 */
	public static boolean isEmpty (String str) {
		if (str == null || "".equals(str)) {
			return true;
		}
		return false;
	}

	/**
	 * is contain (or)
	 * 
	 * @param str
	 * @param target
	 * @return true : target 안의 string중 하나라도 str과 같으면 true
	 */
	public static boolean isContain (String str, String[] target) {
		for (String s : target) {
			if (s.equals(str)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Dec To Hex
	 * 
	 * @param dec
	 *        integer
	 * @return String Hex
	 */
	public static String intToHex (int dec) {
		if ((dec >= 0) && (dec < 16)) {
			return "0" + Integer.toHexString(dec);
		} else {
			return Integer.toHexString(dec);
		}
	}

	private final static String tagStart = "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";
	private final static String tagEnd = "\\</\\w+\\>";
	private final static String tagSelfClosing = "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";
	private final static String htmlEntity = "&[a-zA-Z][a-zA-Z0-9]+;";
	private final static Pattern htmlPattern=Pattern.compile("(" + tagStart + ".*" + tagEnd + ")|(" + tagSelfClosing + ")|(" + htmlEntity + ")", Pattern.DOTALL);

	public static boolean isHtml(String s)
	{
		boolean ret = false;
		if (s != null)
		{
			ret = htmlPattern.matcher(s).find();
		}

		return ret;
	}
}
