package com.pms.sdk.common.util;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author erzisk
 * @since 2013.06.26
 */
public class PMSUtil implements IPMSConsts {

	/**
	 * set mqtt flag
	 * 
	 * @param context
	 * @param mqttFlag
	 */
	public static void setMQTTFlag (final Context context, boolean mqttFlag) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_MQTT_FLAG, mqttFlag ? FLAG_Y : FLAG_N);
	}

	/**
	 * get mqtt flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTFlag (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String mqttFlag = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_MQTT_FLAG");
			return mqttFlag == null || mqttFlag.equals("") ? prefs.getString(PREF_MQTT_FLAG) : mqttFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}

	/**
	 * get private flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateFlag (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_PRIVATE_FLAG);
	}

	/**
	 * get Private Log Flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateLogFlag (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_PRIVATE_LOG_FLAG);
	}

	public static void setMQTTServerUrl (final Context context, String... serverUrl) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_MQTT_SERVER_SSL_URL, serverUrl[0]);
		prefs.putString(PREF_MQTT_SERVER_TCP_URL, serverUrl[1]);
	}

	/**
	 * get mqtt server url
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTServerUrl (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String mqttServerUrl = "";
			String mqttProtocol = getPrivateProtocol(context);
			if (FLAG_Y.equals(prefs.getString(PREF_MQTT_SERVER_CHECK))) {
				if (PROTOCOL_SSL.equals(mqttProtocol)) {
					mqttServerUrl = prefs.getString(PREF_MQTT_SERVER_SSL_URL);
				} else {
					mqttServerUrl = prefs.getString(PREF_MQTT_SERVER_TCP_URL);
				}
			} else {
				if (PROTOCOL_SSL.equals(mqttProtocol)) {
					mqttServerUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
							.get("PMS_MQTT_SERVER_URL_SSL");
				} else {
					mqttServerUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
							.get("PMS_MQTT_SERVER_URL_TCP");
				}
			}
			return mqttServerUrl == null || mqttServerUrl.equals("") ? "" : mqttServerUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getSingKey (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_SSL_SIGN_KEY);
	}

	public static String getSignPass (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_SSL_SIGN_PASS);
	}

	/**
	 * get Private Log Flag
	 * 
	 * @param context
	 * @return
	 */
	public static String getPrivateProtocol (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_PRIVATE_PROTOCOL);
	}

	/**
	 * get mqtt server url
	 * 
	 * @param context
	 * @return
	 */
	public static String getMQTTServerKeepAlive (final Context context) {
		try {
			String keepalive = String
					.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
							.getInt("PMS_MQTT_SERVER_KEEPALIVE"));
			return keepalive == null || keepalive.equals("") ? "400" : keepalive;
		} catch (Exception e) {
			e.printStackTrace();
			return "400";
		}
	}

	public static void setApplicationKey (final Context context, String key) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_APP_KEY, key);
	}

	/**
	 * return application key
	 * 
	 * @param context
	 * @return
	 */
	public static String getApplicationKey (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String appKey = "";
			if (FLAG_Y.equals(prefs.getString(PREF_APP_KEY_CHECK))) {
				appKey = prefs.getString(PREF_APP_KEY);
			} else {
				appKey = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
						.get("PMS_APP_KEY");
				appKey = appKey.trim();
			}
			return appKey;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getBigNotiContextMsg (final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_NOTI_CONTENT");
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getPushPopupActivity (final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_DEFAULT_POPUP_ACTIVITY");
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * return server url <br>
	 * AndroidManifest.xml에 SERVER_URL이 셋팅되어 있지 않거나, <br>
	 * meta data를 불러오는중 exception이 발생하면 <br>
	 * APIVariable.SERVER_URL을 return
	 * 
	 * @param context
	 * @return
	 */
	public static String getServerUrl (final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String serverUrl = "";
			if (FLAG_Y.equals(prefs.getString("pref_api_server_check"))) {
				serverUrl = prefs.getString(PREF_SERVER_URL);
			} else {
				serverUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
						.get("PMS_API_SERVER_URL");
			}

			return serverUrl == null || serverUrl.equals("") ? "" : serverUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static int getIconId (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_ICON");

			return iconResId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static int getLargeIconId (final Context context) {
		try {
			int iconResId = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_SET_LARGE_ICON");

			return iconResId;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * set server url
	 * 
	 * @param context
	 * @param serverUrl
	 */
	public static void setServerUrl (final Context context, String serverUrl) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_SERVER_URL, serverUrl);
	}

	/**
	 * set gcm project id
	 * 
	 * @param context
	 * @param projectId
	 */
	public static void setGCMProjectId (final Context context, String projectId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(KEY_GCM_PROJECT_ID, projectId);
	}

	/**
	 * get gcm project id
	 * 
	 * @param context
	 * @return
	 */
	public static String getGCMProjectId (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(KEY_GCM_PROJECT_ID);
	}

	/**
	 * set gcm token
	 * 
	 * @param context
	 * @param gcmToken
	 */
	public static void setGCMToken (final Context context, String gcmToken) {
		Prefs prefs = new Prefs(context);
		prefs.putString(KEY_GCM_TOKEN, gcmToken);
	}

	/**
	 * return gcm token (push)
	 * 
	 * @param context
	 * @return
	 */
	public static String getGCMToken (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(KEY_GCM_TOKEN);
	}

	/**
	 * get createtoken flag
	 *
	 * @param context
	 * @return
	 */
	public static String getEnableUUIDFlag(final Context context) {
		try {
			String uuidFlag = (String) context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_ENABLE_UUID");
			return (uuidFlag == null) ? FLAG_Y : uuidFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}


	/**
	 * set UUID
	 *
	 * @param context
	 * @param gcmToken
	 */
	public static void setUUID (final Context context, String gcmToken) {
		// 20170811 fixed by hklim
		// UUID 저장 시에 Shared Preference 를 사용하지 않고 DB 사용 하도록 수정됨
		DataKeyUtil.setDBKey(context, DB_UUID, gcmToken);
	}

	/**
	 * get UUID
	 *
	 * @param context
	 * @return
	 */
	public static String getUUID (final Context context) {
		String strReturn = DataKeyUtil.getDBKey(context, DB_UUID);
		return strReturn;
	}


	/**
	 * set encrypt key
	 * 
	 * @param context
	 * @param encKey
	 */
	public static void setEncKey (final Context context, String encKey) {
//		Prefs prefs = new Prefs(context);
//		prefs.putString(PREF_ENC_KEY, encKey);
		DataKeyUtil.setDBKey(context, PREF_ENC_KEY, encKey);
	}

	/**
	 * get encrypt key
	 * 
	 * @param context
	 * @return
	 */
	public static String getEncKey (final Context context) {
		String value = DataKeyUtil.getDBKey(context, PREF_ENC_KEY);
		if(TextUtils.isEmpty(value))
		{
			Prefs prefs = new Prefs(context);
			value = prefs.getString(PREF_ENC_KEY);
		}
		return value;
	}

	/**
	 * set appUserId
	 * 
	 * @param context
	 * @param appUserId
	 */
	public static void setAppUserId (final Context context, String appUserId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_APP_USER_ID, appUserId);
	}

	/**
	 * get appUserId
	 * 
	 * @param context
	 * @return
	 */
	public static String getAppUserId (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_APP_USER_ID);
	}

	/**
	 * set cust id
	 * 
	 * @param context
	 * @param custId
	 */
	public static void setCustId (final Context context, String custId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_CUST_ID, custId);
	}

	/**
	 * get cust id
	 * 
	 * @param context
	 * @return
	 */
	public static String getCustId (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_CUST_ID);
	}

	public static void setPopupActivity (Context context, Boolean state) {
		Prefs prefs = new Prefs(context);
		prefs.putBoolean(PREF_ISPOPUP_ACTIVITY, state);
	}

	public static Boolean getPopupActivity (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getBoolean(PREF_ISPOPUP_ACTIVITY);
	}

	public static void setNotiOrPopup (Context context, Boolean isNotiPopup) {
		Prefs prefs = new Prefs(context);
		prefs.putBoolean(PREF_NOTIORPOPUP_SETTING, isNotiPopup);
	}

	public static Boolean getNotiOrPopup (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getBoolean(PREF_NOTIORPOPUP_SETTING);
	}

	public static void setIsCustNoti (Context context, Boolean isCustNoti) {
		Prefs prefs = new Prefs(context);
		prefs.putBoolean(PREF_IS_CUST_NOTI, isCustNoti);
	}

	public static Boolean getIsCustNoti (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getBoolean(PREF_IS_CUST_NOTI);
	}

	public static void setNotiResId (Context context, int resid) {
		Prefs prefs = new Prefs(context);
		prefs.putInt(PREF_NOTI_RES_ID, resid);
	}

	public static int getNotiResId (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getInt(PREF_NOTI_RES_ID);
	}

	public static void setNotiTitleResId (Context context, int resid) {
		Prefs prefs = new Prefs(context);
		prefs.putInt(PREF_NOTI_TITLE_RES_ID, resid);
	}

	public static int getNotiTitleResId (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getInt(PREF_NOTI_TITLE_RES_ID);
	}

	public static void setNotiContentResId (Context context, int resid) {
		Prefs prefs = new Prefs(context);
		prefs.putInt(PREF_NOTI_CONTENT_RES_ID, resid);
	}

	public static int getNotiContenResId (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getInt(PREF_NOTI_CONTENT_RES_ID);
	}

	public static void setFirstTime (Context context, String time) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_FIRST_TIME, time);
	}

	public static String getFirstTime (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_FIRST_TIME);
	}

	public static void setSecondTime (Context context, String time) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_SECOND_TIME, time);
	}

	public static String getSecondTime (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_SECOND_TIME);
	}

	public static void setAllowedTime (Context context, Boolean flag) {
		Prefs prefs = new Prefs(context);
		prefs.putBoolean(PREF_ALLOWED_FLAG, flag);
	}

	public static Boolean getAllowedTime (Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getBoolean(PREF_ALLOWED_FLAG);
	}

	/**
	 * set DeviceCert Status
	 * 
	 * @param context
	 * @param status
	 */
	public static void setDeviceCertStatus (final Context context, String status) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_DEVICECERT_STATUS, status);
	}

	/**
	 * get DeviceCert Status
	 * 
	 * @param context
	 * @return
	 */
	public static String getDeviceCertStatus (final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_DEVICECERT_STATUS);
	}

	/**
	 * get Link From html
	 * 
	 * @param htmlStr
	 * @return
	 */
	@SuppressLint("UseSparseArrays")
	public static Map<Integer, String> getLinkFromHtml (String htmlStr) {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)([^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			map.put(i, m.group(2));
		}
		return map;
	}

	/**
	 * replaceLink
	 * 
	 * @param htmlStr
	 * @return
	 */
	public static String replaceLink (String htmlStr) {
		StringBuffer s = new StringBuffer();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)([^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			m.appendReplacement(s, m.group(0).replace(m.group(2), "click://" + i));
		}
		m.appendTail(s);
		return s.toString();
	}

	/**
	 * find top activity
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String[] findTopActivity (Context c) {
		String[] top = new String[2];
		ActivityManager activityManager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> info;
		info = activityManager.getRunningTasks(1);
		for (Iterator<RunningTaskInfo> iterator = info.iterator(); iterator.hasNext();) {
			RunningTaskInfo runningTaskInfo = iterator.next();

			top[0] = runningTaskInfo.topActivity.getPackageName();
			top[1] = runningTaskInfo.topActivity.getClassName();
		}

		return top;
	}

	/**
	 * runnedApp 현재 어플리케이션이 실행중인지 여부
	 * 
	 * @return
	 */
	public static boolean isRunnedApp (Context c) {
		if (findTopActivity(c)[0].equals(c.getPackageName())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * is screen on
	 * 
	 * @param c
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean isScreenOn (Context c) {
		PowerManager pm;
		try {
			pm = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		} catch (Exception e) {
			return false;
		} finally {
			PMS.clear();
		}
	}

	public static JSONObject getReadParam (String msgId) {
		JSONObject read = new JSONObject();
		try {
			read.put("msgId", msgId);
			read.put("workday", DateUtil.getNowDate());
			return read;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * array to preferences
	 * 
	 * @param obj
	 */
	public static void arrayToPrefs (Context c, String key, Object obj) {
		Prefs prefs = new Prefs(c);
		String arrayString = prefs.getString(key);
		JSONArray array = null;
		try {
			if ("".equals(arrayString)) {
				array = new JSONArray();
			} else {
				array = new JSONArray(arrayString);
			}
			array.put(obj);
			prefs.putString(key, array.toString());
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	/**
	 * array from preferences
	 * 
	 * @param c
	 * @param key
	 * @return
	 */
	public static JSONArray arrayFromPrefs (Context c, String key) {
		Prefs prefs = new Prefs(c);
		String arrayString = prefs.getString(key);
		JSONArray array = new JSONArray();
		try {
			array = new JSONArray(arrayString);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return array;
	}

	public static int getTargetVersion(final Context context) {
		try {
			int numTargetVersion = context.getPackageManager().getApplicationInfo(
					context.getPackageName(), 0).targetSdkVersion;
			return numTargetVersion;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static String getApplicationName(Context context) {
		PackageManager packageManager = context.getPackageManager();
		ApplicationInfo applicationInfo = null;
		try {
			applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
		}
		catch (final NameNotFoundException e) {
		}
		finally {
			return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : context.getPackageName());
		}
	}

	public static boolean getNotiBigTextMode(Context context)
	{
		Prefs prefs = new Prefs(context);
		String mode = prefs.getString(PRO_BIG_TEXT_MODE);
		if (!TextUtils.isEmpty(mode))
		{
			if (FLAG_Y.equals(mode))
			{
				return true;
			}
		}

		return false;
	}

	public static void setNotiBigTextMode (Context context, boolean flag)
	{
		Prefs prefs = new Prefs(context);
		String mode = FLAG_N;
		if (flag)
		{
			mode = FLAG_Y;
		}

		prefs.putString(PRO_BIG_TEXT_MODE, mode);
	}

	public static void setNotificationClickActivityAction(Context context, String action) {
		if (!TextUtils.isEmpty(action)) {
			DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_ACTION, action);
		}
	}

	public static void setNotificationClickActivityClass(Context context, Class receiver) {
		if (receiver != null) {
			DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_CLASS,
			                     receiver.getCanonicalName());
		} else {
			DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_CLASS, "");
		}
	}

	public static void setNotificationClickActivityUseBackStack(Context context, boolean value) {
		if (value) {
			DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_USE_BACKSTACK, FLAG_Y);
		} else {
			DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_USE_BACKSTACK, FLAG_N);
		}
	}

	public static String getNotificationClickActivityAction(Context context) {
		String action = DataKeyUtil.getDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_ACTION);
		if (!TextUtils.isEmpty(action)) {
			return action;
		}

		action = ProPertiesFileUtil.getString(context, PRO_NOTIFICATION_CLICK_ACTIVITY_ACTION);
		if(!TextUtils.isEmpty(action)) {
			return action;
		} else {
			return "com.tms.sdk.notification." + context.getPackageName();
		}
	}

	public static String getNotificationClickActivityClass(Context context) {
		String receiver = DataKeyUtil.getDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_CLASS);
		if (!TextUtils.isEmpty(receiver)) {
			return receiver;
		}

		receiver = ProPertiesFileUtil.getString(context, PRO_NOTIFICATION_CLICK_ACTIVITY_CLASS);
		if(!TextUtils.isEmpty(receiver)) {
			return receiver;
		} else {
			return null;
		}
	}

	public static boolean getNotificationClickActivityUseBackStack(Context context) {
		String value = DataKeyUtil.getDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_USE_BACKSTACK);
		boolean result = false;
		if (!TextUtils.isEmpty(value)) {
			return FLAG_Y.equals(value);
		}

		value = ProPertiesFileUtil.getString(context, PRO_NOTIFICATION_CLICK_ACTIVITY_USE_BACKSTACK);
		if(!TextUtils.isEmpty(value)){
			return FLAG_Y.equals(value);
		} else {
			return false;
		}
	}

	public static void setNotificationClickActivityFlag(Context context, int flags) {
		DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_FLAGS,
		                     String.format("%d", flags));
	}

	public static String getNotificationClickActivityFlag(Context context) {
		String value = DataKeyUtil.getDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_FLAGS);
		if (!TextUtils.isEmpty(value)) {
			return value;
		}

		value = ProPertiesFileUtil.getString(context, PRO_NOTIFICATION_CLICK_ACTIVITY_FLAGS);
		if(!TextUtils.isEmpty(value)) {
			return value;
		} else {
			return null;
		}
	}

	public static void setNotificationClickActivityFlagEnable(Context context, boolean value) {
		DataKeyUtil.setDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_FLAGS_ENABLE,
		                     value ? FLAG_Y : FLAG_N);
	}

	public static boolean getNotificationClickActivityFlagEnable(Context context) {
		String value = DataKeyUtil.getDBKey(context, DB_NOTIFICATION_CLICK_ACTIVITY_FLAGS_ENABLE);
		if (!TextUtils.isEmpty(value)) {
			return FLAG_Y.equals(value);
		}

		value = ProPertiesFileUtil.getString(context, PRO_NOTIFICATION_CLICK_ACTIVITY_FLAGS_ENABLE);
		if(!TextUtils.isEmpty(value)) {
			return FLAG_Y.equals(value);
		} else {
			return false;
		}
	}

	public static String getNotificationChannelId(Context context) {
		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}
		CLog.d("DB_NOTI_CHANNEL_ID " + strNotiChannel);
		return strNotiChannel;
	}

	public static void createNotificationChannel(Context context){
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
			if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1) {
				String notificationChannelId = PMSUtil.getNotificationChannelId(context);
				PMSUtil.createNotiChannel(context, notificationChannelId);
				CLog.i("notificationChannel [" + notificationChannelId + "] was created.");
			} else {
				CLog.i("notificationChannel was not created. TargetSDK Version is lower than 26.");
			}
		} else {
			CLog.i("notificationChannel was not created. Device Version is lower than 26.");
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private static String createNotiChannel(Context context, String channelId) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(
				Context.NOTIFICATION_SERVICE);
		NotificationChannel notiChannel = notificationManager.getNotificationChannel(channelId);
		Prefs mPrefs = new Prefs(context);
		boolean isShowBadge = false;
		boolean isPlaySound;
		boolean isEnableVibe;

		try {
			if (IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(
					context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get(
					IPMSConsts.META_DATA_NOTI_O_BADGE))) {
				isShowBadge = true;
			} else {
				isShowBadge = false;
			}
		} catch (PackageManager.NameNotFoundException e) {
			CLog.e(e.getMessage());
		}

		if (IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG))) {
			isPlaySound = true;
		} else {
			isPlaySound = false;
		}
		if (IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG))) {
			isEnableVibe = true;
		} else {
			isEnableVibe = false;
		}
		try {
			CLog.d("AppSetting isShowBadge " + context.getPackageManager().getApplicationInfo(
					context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE) +
					       " isPlaySound " + mPrefs.getString(PREF_RING_FLAG) +
					       " isEnableVibe " + mPrefs.getString(PREF_VIBE_FLAG));
		} catch (PackageManager.NameNotFoundException e) {
			CLog.e(e.getMessage());
		}

		if (notiChannel == null) {    //if notichannel is not initialized
			CLog.d("notification channel is initialized / " + channelId);
			notiChannel = new NotificationChannel(channelId, PMSUtil.getApplicationName(context),
			                                      NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setShowBadge(isShowBadge);
			notiChannel.enableLights(true);
			notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
			notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
			notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setBypassDnd(true);

			notiChannel.enableVibration(isEnableVibe);
			if (isEnableVibe) {
				notiChannel.setVibrationPattern(new long[]{0, 1000});
			}

			if (isPlaySound) {
				Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				try {
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0) {
						uri = Uri.parse(
								ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						CLog.d("notiSound " + notiSound + " uri " + uri.toString());
					} else {
						throw new Exception("default ringtone is set");
					}
				} catch (Exception e) {
					uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					CLog.e(e.getMessage());
				}

				AudioAttributes audioAttributes = new AudioAttributes.Builder()
						.setUsage(AudioAttributes.USAGE_NOTIFICATION)
						.build();
				notiChannel.setSound(uri, audioAttributes);
				CLog.d("setChannelSound ring with initialize notichannel");
			} else {
				notiChannel.setSound(null, null);
			}
		} else {
			CLog.d("notification channel is exist " + notiChannel);
		}

		String name = mPrefs.getString(IPMSConsts.PREF_NOTIFICATION_CHANNEL_NAME);
		if (TextUtils.isEmpty(name)) {
			name = PMSUtil.getApplicationName(context);
		}
		notiChannel.setName(name);
		String description = mPrefs.getString(IPMSConsts.PREF_NOTIFICATION_CHANNEL_DESCRIPTION);
		if (!TextUtils.isEmpty(description)) {
			notiChannel.setDescription(description);
		}

		notificationManager.createNotificationChannel(notiChannel);
		DataKeyUtil.setDBKey(context, DB_NOTI_CHANNEL_ID, channelId);
		return channelId;
	}

	public static boolean hasFirebaseInstanceIdApi() {
		try {
			String classPath = "com.google.firebase.iid.FirebaseInstanceId";
			Class<?> cls = Class.forName(classPath);
			if(cls != null) {
				return true;
			}
			else {
				return false;
			}
		}
		catch (Exception e) {
			CLog.i("com.google.firebase.iid doesn't exist");
			return false;
		}
	}
}
