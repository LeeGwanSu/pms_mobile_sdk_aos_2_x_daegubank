package com.pms.sdk.common.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.pms.sdk.IPMSConsts;

/**
 *
 * @author erzisk
 * @since 2013.06.11
 */
public class Prefs implements IPMSConsts
{

	private SharedPreferences prefs;
	private Editor edit;

	public Prefs(Context context) {
		prefs = context.getSharedPreferences(PREF_FILE_NAME, Activity.MODE_MULTI_PROCESS);
		edit = prefs.edit();
	}

	public void putString (String k, String v) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return;
		}
		if(v == null)
		{
			CLog.e("[Prefs] value cannot be null");
			return;
		}
		try {
			edit.putString(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}
		edit.commit();
	}

	public String getString (String k) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return "";
		}
		try {
			return prefs.getString(k, "");
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public void putInt (String k, int v) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return;
		}
		try {
			edit.putInt(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}
		edit.commit();
	}

	public int getInt (String k) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return -1;
		}
		try {
			return prefs.getInt(k, -1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;

	}

	public int getInt (String k, int def) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return -1;
		}
		try {
			return prefs.getInt(k, def);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;

	}

	public void putLong (String k, long v) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return ;
		}
		try {
			edit.putLong(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}
		edit.commit();
	}

	public Long getLong (String k) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return (long)-1;
		}
		try {
			return prefs.getLong(k, -1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (long) -1;
	}

	public void putBoolean (String k, boolean v) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return ;
		}
		try {
			edit.putBoolean(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}

		edit.commit();
	}

	public Boolean getBoolean (String k) {
		if(k == null)
		{
			CLog.e("[Prefs] key cannot be null");
			return false;
		}
		try {
			return prefs.getBoolean(k, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
