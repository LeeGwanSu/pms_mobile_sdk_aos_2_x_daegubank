package com.pms.sdk.common.util;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @since 2012.01.14
 * @author erzisk
 * @description date util
 */
public class DateUtil {

	public static final String DEFAULT_FORMAT = "yyyyMMddHHmmss";
	public static final String DATE_FORMAT = "yyyyMMdd";
	public static final String TIME_FORMAT = "HHmmss";

	@SuppressLint("SimpleDateFormat")
	public static String getNowDate () {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(DEFAULT_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		// sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		return sdf.format(new Date().getTime());
	}

	@SuppressLint("SimpleDateFormat")
	public static String getNowDateMo () {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		return sdf.format(new Date().getTime());
	}

	@SuppressLint("SimpleDateFormat")
	public static String getNowTime () {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(TIME_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		String time = sdf.format(new Date().getTime());
		time = getTimeChange(time, "24", "00");
		return time;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getYesterdayDate () {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		long lCurTime = new Date().getTime();
		return sdf.format(new Date(lCurTime + (1000 * 60 * 60 * 24 * -1)));
	}

	@SuppressLint("SimpleDateFormat")
	public static String getTomorrowDate (String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date torDate = null;
		try {
			torDate = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long lCurTime = torDate.getTime();
		return sdf.format(new Date(lCurTime + (1000 * 60 * 60 * 24 * +1)));
	}

	@SuppressLint("SimpleDateFormat")
	public static Boolean getCompareDate (String time1, String time2) {
		DateFormat dateFormat = new SimpleDateFormat(DEFAULT_FORMAT);
		try {
			String temp = getTimeChange(time1, "24", "00");

			Date nowdate = dateFormat.parse(getNowDate());
			Date afterdate = dateFormat.parse(getNowDateMo() + temp);
			Date beforedate = null;

			if (Integer.parseInt(temp) > Integer.parseInt(time2)) {
				beforedate = dateFormat.parse(getTomorrowDate(getNowDateMo()) + time2);
			} else {
				beforedate = dateFormat.parse(getNowDateMo() + time2);
			}

			Boolean state = nowdate.after(afterdate);
			Boolean state1 = nowdate.before(beforedate);

			if ((state == true) && (state1 == true)) {
				return false;
			} else {
				return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressLint("SimpleDateFormat")
	public static long diffOfDate (String start, String end) {
		long diffDays = 0;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);

			Date startDate = formatter.parse(start);
			Date endDate = formatter.parse(end);

			long diff = endDate.getTime() - startDate.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return diffDays;
	}

	@SuppressLint("SimpleDateFormat")
	public static long diffOfTime (String start, String end, String date) {
		long diffTime = 0;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_FORMAT);

			Date startDate = formatter.parse(date + getTimeChange(start, "00", "24"));
			Date endDate = formatter.parse(date + getTimeChange(end, "00", "24"));

			diffTime = endDate.getTime() - startDate.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (diffTime / 1000);
	}

	private static String getTimeChange (String date, String after, String before) {
		int index = date.indexOf(after);
		if ((index != -1) && (index == 0)) {
			String temp = "";
			temp = date.substring(2);
			date = before + temp;
		}

		return date;
	}

	/**
	 * 두 문자열을 비교하여 지난 날인지 여부 체크
	 * 
	 * @param before
	 * @param last
	 * @return last날짜가 before 날짜보다 지났으면 true 아니면 false를 리턴
	 * @throws Exception
	 */
	@SuppressLint("SimpleDateFormat")
	public static boolean isDateAfter (String before, String last) throws Exception {
		boolean result = false;

		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		SimpleDateFormat format2 = new SimpleDateFormat(DATE_FORMAT);

		try {
			Date d1 = format.parse(before);
			Date d2 = format2.parse(last);

			if (d2.after(d1)) {
				return true;
			}

		} catch (Exception e) {
			throw new Exception("error " + e);
		}
		return result;
	}

	/**
	 * 두 문자열을 비교하여 지난 시간인지 여부 체크
	 * 
	 * @param before
	 * @param last
	 * @return last날짜가 before 날짜보다 지났으면 true 아니면 false를 리턴
	 * @throws Exception
	 */
	@SuppressLint("SimpleDateFormat")
	public static boolean isTimeAfter (String before, String last) throws Exception {
		boolean result = false;

		SimpleDateFormat format = new SimpleDateFormat(DEFAULT_FORMAT);
		SimpleDateFormat format2 = new SimpleDateFormat(DEFAULT_FORMAT);

		try {
			Date d1 = format.parse(before);
			Date d2 = format2.parse(last);

			if (d2.after(d1)) {
				return true;
			}

		} catch (Exception e) {
			throw new Exception("error " + e);
		}
		return result;
	}

	@SuppressLint("SimpleDateFormat")
	public static long changeDateToMilliSecond (String dateStr) {
		if (dateStr == null || dateStr.trim().equals("") || dateStr.length() != 14) {
			return 0;
		}
		try {
			SimpleDateFormat format = new SimpleDateFormat(DEFAULT_FORMAT);
			return format.parse(dateStr).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * SimpleDateFormat을 사용하여 date converting
	 * 
	 * @param date
	 * <br>
	 *        ex) "20120812230908"
	 * @param inputFromat
	 * <br>
	 *        ex) "yyyyMMddkkmmss"
	 * @param outputFormat
	 * <br>
	 *        ex) "yyyy년 M월 d일 E요일"
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String convertDate (String date, String inputFromat, String outputFormat) {
		SimpleDateFormat originalFormat = new SimpleDateFormat(inputFromat);
		SimpleDateFormat newFormat = new SimpleDateFormat(outputFormat);

		try {
			Date originalDate = originalFormat.parse(date);
			String temp = newFormat.format(originalDate);
			/*
			 * String time = temp.substring(11); time = getTimeChange(time, "24", "00"); return temp.substring(0, 11) + time;
			 */
			return temp;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressLint("SimpleDateFormat")
	public static String convertDate (Calendar c, String outputFormat) {
		try {
			SimpleDateFormat newFormat = new SimpleDateFormat(outputFormat);
			return newFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * convert chat date
	 * 
	 * @param timeString
	 * <br>
	 *        ex)20120101010101
	 * @return 날짜가 현재와 같을 경우 : Am 01:01 or Pm 01:01 <br>
	 *         날짜가 현재와 다를 경우 : 2012.01.01
	 */
	public static String convertChatDate (String timeString) {
		try {
			if (timeString.substring(0, 8).equals(getNowDate().substring(0, 8))) {
				return convertDate(timeString, DEFAULT_FORMAT, "aa hh:mm");
			} else {
				return convertDate(timeString, DEFAULT_FORMAT, "yyyy년 MM월 dd일");
			}
		} catch (StringIndexOutOfBoundsException e) {
			return "";
		}

	}

	/**
	 * 현재 날짜에서 30일 더한값 가져오는 Method
	 * 
	 * @param date
	 *        설정할 날짜 값.
	 * @return 30일 더한갑
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getAfterOnMonth (String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date torDate = null;
		try {
			torDate = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(torDate);
		cal.add(Calendar.DATE, 1);

		return sdf.format(cal.getTime());
	}

	@SuppressLint("SimpleDateFormat")
	public static String getMillisecondsToDate (long time) {
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String str = dayTime.format(new Date(time));
		return str;
	}

	public static boolean isDoNotDisturb(Date fromDate, Date toDate, Date currentDate)
	{
		boolean isDisturb = false;
		Date fromDateReference = new Date();
		fromDateReference.setHours(fromDate.getHours());
		Date toDateReference = new Date();
		toDateReference.setHours(toDate.getHours());

		if(toDate.before(fromDate))
		{
			toDate.setDate(toDate.getDate()+1);
		}

		fromDateReference.setTime(fromDate.getTime()-(1*60*1000));
		toDateReference.setTime(toDate.getTime()+(1*60*1000));

		if(currentDate.before(fromDate) && toDate.getDate() >= fromDate.getDate()+1)
		{
			currentDate.setDate(currentDate.getDate()+1);
		}
		if(currentDate.after(fromDateReference) && currentDate.before(toDateReference))
		{
			isDisturb = true;
		}
		SimpleDateFormat format = new SimpleDateFormat("MM/dd HH:mm");

		CLog.i("DoNotDisturbTime -> "+isDisturb+" " + format.format(fromDate)+ " ~ " + format.format(toDate)+" currentTime " + format.format(currentDate));
//		CLog.i("temp "+format.format(fromDateReference)+"~"+format.format(toDateReference));
		return isDisturb;
	}
	public static boolean isDoNotDisturb(String from, String to, String current)
	{
		try
		{
			int fromHour = Integer.parseInt(from.substring(0, 2));
			int fromMinute = Integer.parseInt(from.substring(2, 4));
			int toHour = Integer.parseInt(to.substring(0, 2));
			int toMinute = Integer.parseInt(to.substring(2, 4));
			int currentHour = Integer.parseInt(current.substring(0, 2));
			int currentMinute = Integer.parseInt(current.substring(2, 4));
			Date fromDate = new Date();
			fromDate.setHours(fromHour);
			fromDate.setMinutes(fromMinute);
			Date toDate = new Date();
			toDate.setHours(toHour);
			toDate.setMinutes(toMinute);
			Date currentDate = new Date();
			currentDate.setHours(currentHour);
			currentDate.setMinutes(currentMinute);
			return isDoNotDisturb(fromDate, toDate, currentDate);
		}
		catch (Exception e)
		{
			CLog.e(e.toString());
			return false;
		}
	}
}