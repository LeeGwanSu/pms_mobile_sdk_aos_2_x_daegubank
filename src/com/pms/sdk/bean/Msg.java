package com.pms.sdk.bean;

import org.json.JSONObject;

import android.database.Cursor;

/**
 * @since 2012.12.26
 * @author erzisk
 * @description message bean
 */
public class Msg {

	public static final String TABLE_NAME = "TBL_MSG";
	public static final String _ID = "_id";
	public static final String USER_MSG_ID = "USER_MSG_ID";
	public static final String MSG_GRP_NM = "MSG_GRP_NM";
	public static final String APP_LINK = "APP_LINK";
	public static final String ICON_NAME = "ICON_NAME";
	public static final String MSG_ID = "MSG_ID";
	public static final String MSG_KIND = "MSG_KIND";
	public static final String PUSH_TITLE = "PUSH_TITLE";
	public static final String PUSH_MSG = "PUSH_MSG";
	public static final String MSG_TEXT = "MSG_TEXT";
	public static final String MAP1 = "MAP1";
	public static final String MAP2 = "MAP2";
	public static final String MAP3 = "MAP3";
	public static final String MSG_TYPE = "MSG_TYPE";
	public static final String READ_YN = "READ_YN";
	public static final String EXPIRE_DATE = "EXPIRE_DATE";
	public static final String REG_DATE = "REG_DATE";
	public static final String REG_UID = "REG_UID";
	public static final String MSG_GRP_CD = "MSG_GRP_CD";
	public static final String RECOVERY_FLAG = "RECOVERY_FLAG";

	// [code flag]
	// default
	public static final String CODE_DEFAULT = "00000";

	// [type flag]
	// text
	public static final String TYPE_T = "T";
	// attach
	public static final String TYPE_A = "A";
	// html
	public static final String TYPE_H = "H";
	// link
	public static final String TYPE_L = "L";

	// [read falg]
	// read
	public static final String FLAG_Y = "Y";
	// unread
	public static final String FLAG_N = "N";

	public static final int UID_FIRST_LOCATION = 1;

	public static final int UID_LENGTH = 4;

	public static final String TIME_MILLISECOND = "000";

	public static final int ROW_COUNT = 50;

	public static final String CREATE_MSG = "CREATE TABLE " + TABLE_NAME + "( " + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USER_MSG_ID
			+ " INTEGER, " + MSG_GRP_NM + " TEXT, " + APP_LINK + " TEXT, " + ICON_NAME + " TEXT, " + MSG_ID + " INTEGER, " + MSG_KIND + " TEXT, "
			+ PUSH_TITLE + " TEXT, " + PUSH_MSG + " TEXT, " + MSG_TEXT + " TEXT, " + MAP1 + " TEXT, " + MAP2 + " TEXT, " + MAP3 + " TEXT, "
			+ MSG_TYPE + " TEXT, " + READ_YN + " TEXT, " + EXPIRE_DATE + " TEXT, " + REG_DATE + " TEXT, " + REG_UID + " TEXT, " + MSG_GRP_CD
			+ " TEXT, " + RECOVERY_FLAG + " TEXT " + ");";

	public String id = "-1";
	public String userMsgId = "";
	public String msgGrpNm = "";
	public String appLink = "";
	public String iconName = "";
	public String msgId = "";
	public String msgKind = "";
	public String pushTitle = "";
	public String pushMsg = "";
	public String msgText = "";
	public String map1 = "";
	public String map2 = "";
	public String map3 = "";
	public String msgType = "";
	public String readYn = "";
	public String expireDate = "";
	public String regDate = "";
	public String reqUid = "";
	public String msgGrpCd = "";
	public String recoveryFlag = "N";

	public Msg() {
	}

	public Msg(Cursor c) {
		// Convert.cursorToBean(c, this);

		id = c.getString(c.getColumnIndexOrThrow(_ID));
		userMsgId = c.getString(c.getColumnIndexOrThrow(USER_MSG_ID));
		msgGrpNm = c.getString(c.getColumnIndexOrThrow(MSG_GRP_NM));
		appLink = c.getString(c.getColumnIndexOrThrow(APP_LINK));
		iconName = c.getString(c.getColumnIndexOrThrow(ICON_NAME));
		msgId = c.getString(c.getColumnIndexOrThrow(MSG_ID));
		msgKind = c.getString(c.getColumnIndexOrThrow(MSG_KIND));
		pushTitle = c.getString(c.getColumnIndexOrThrow(PUSH_TITLE));
		pushMsg = c.getString(c.getColumnIndexOrThrow(PUSH_MSG));
		msgText = c.getString(c.getColumnIndexOrThrow(MSG_TEXT));
		map1 = c.getString(c.getColumnIndexOrThrow(MAP1));
		map2 = c.getString(c.getColumnIndexOrThrow(MAP2));
		map3 = c.getString(c.getColumnIndexOrThrow(MAP3));
		msgType = c.getString(c.getColumnIndexOrThrow(MSG_TYPE));
		readYn = c.getString(c.getColumnIndexOrThrow(READ_YN));
		expireDate = c.getString(c.getColumnIndexOrThrow(EXPIRE_DATE));
		regDate = c.getString(c.getColumnIndexOrThrow(REG_DATE));
		reqUid = c.getString(c.getColumnIndexOrThrow(REG_UID));
		msgGrpCd = c.getString(c.getColumnIndexOrThrow(MSG_GRP_CD));
		recoveryFlag = c.getString(c.getColumnIndexOrThrow(RECOVERY_FLAG));
	}

	public Msg(JSONObject jo) {
		try {
			if (jo.has("userMsgId")) {
				userMsgId = jo.getString("userMsgId");
			}
			if (jo.has("msgGrpNm")) {
				msgGrpNm = jo.getString("msgGrpNm");
			}
			if (jo.has("appLink")) {
				appLink = jo.getString("appLink");
			}
			if (jo.has("iconName")) {
				iconName = jo.getString("iconName");
			}
			if (jo.has("msgId")) {
				msgId = jo.getString("msgId");
			}
			if (jo.has("msgKind")) {
				msgKind = jo.getString("msgKind");
			}
			if (jo.has("pushTitle")) {
				pushTitle = jo.getString("pushTitle");
			}
			if (jo.has("pushMsg")) {
				pushMsg = jo.getString("pushMsg");
			}
			if (jo.has("msgText")) {
				msgText = jo.getString("msgText");
			}
			if (jo.has("map1")) {
				map1 = jo.getString("map1");
			}
			if (jo.has("map2")) {
				map2 = jo.getString("map2");
			}
			if (jo.has("map3")) {
				map3 = jo.getString("map3");
			}
			if (jo.has("msgType")) {
				msgType = jo.getString("msgType");
			}
			if (jo.has("readYn")) {
				readYn = jo.getString("readYn");
			}
			if (jo.has("expireDate")) {
				expireDate = jo.getString("expireDate");
			}
			if (jo.has("regDate")) {
				regDate = jo.getString("regDate");
			}
			if (jo.has("reqUid")) {
				reqUid = jo.getString("reqUid");
			}
			if (jo.has("msgGrpCd")) {
				msgGrpCd = jo.getString("msgGrpCd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}