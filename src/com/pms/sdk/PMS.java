package com.pms.sdk;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

import java.io.Serializable;

import androidx.annotation.RequiresApi;

import static com.pms.sdk.push.PushReceiver.NOTIFICATION_CHANNEL_DO_NOT_DISTURB;

public class PMS implements IPMSConsts, Serializable {

    private static final long serialVersionUID = 1L;
    private static PMS instancePms;
    private static PMSPopup instancePmsPopup;
    private final PMSDB mDB;
    private final Prefs mPrefs;
    private Context mContext;
    private OnReceivePushListener onReceivePushListener;

    /**
     * constructor
     *
     * @param context
     */
    private PMS(Context context) {
        this.mDB = PMSDB.getInstance(context);
        this.mPrefs = new Prefs(context);
        CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

        initOption(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @return
     */
    public static PMS getInstance(Context context)
    {
        return getInstance(context, "");
    }

    /**
     * getInstance
     *
     * @param context
     * @param projectId
     * @return
     */
    public static PMS getInstance(final Context context, String projectId)
    {
        if (instancePms == null)
        {
            instancePms = new PMS(context);
        }

        instancePms.setmContext(context);
        if (!TextUtils.isEmpty(projectId))
        {
            PMSUtil.setGCMProjectId(context, projectId);
            CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
        }

        return instancePms;
    }

    public static PMSPopup getPopUpInstance() {
        return instancePmsPopup;
    }

    /**
     * clear
     *
     * @return
     */
    public static boolean clear() {
        try {
            PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
            instancePms.unregisterReceiver();
            instancePms = null;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * initialize option
     */
    private void initOption(Context context) {
        if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
            mPrefs.putString(PREF_RING_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            mPrefs.putString(PREF_VIBE_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
            mPrefs.putString(PREF_ALERT_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
            mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
            mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
            mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
        }
        if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
            mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
        }
    }

    /**
     * start mqtt service
     */
    public void startMQTTService(Context context) {
        if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
            context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
        } else {
            context.stopService(new Intent(context, MQTTService.class));
        }
    }

    public void stopMQTTService(Context context) {
        context.stopService(new Intent(context, MQTTService.class));
    }

    private void setmContext(Context context) {
        this.mContext = context;
    }

    private void unregisterReceiver() {
        Badge.getInstance(mContext).unregisterReceiver();
    }

    // /**
    // * push token 세팅
    // * @param pushToken
    // */
    public void setPushToken(String pushToken) {
        CLog.d("setPushToken:pushToken=" + pushToken);
        PMSUtil.setGCMToken(mContext, pushToken);
    }

    public void setPopupSetting(Boolean state, String title) {
        instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
    }

    public String getLastUserMsgId()
    {
        String lastUserMsgId = mDB.selectLastUserMsgId();
        if(TextUtils.isEmpty(lastUserMsgId))
        {
            return "-1";
        }
        else
        {
            return lastUserMsgId;
        }
    }

    /**
     * get cust id
     *
     * @return
     */
    public String getCustId() {
        return PMSUtil.getCustId(mContext);
    }

    /**
     * cust id 세팅
     *
     * @param custId
     */
    public void setCustId(String custId) {
        CLog.i("setCustId");
        CLog.d("setCustId:custId=" + custId);
        PMSUtil.setCustId(mContext, custId);
    }

    public String getFirstTime() {
        return PMSUtil.getFirstTime(mContext);
    }

    public void setFirstTime(String time) {
        PMSUtil.setFirstTime(mContext, time);
    }

    public String getSecondTime() {
        return PMSUtil.getSecondTime(mContext);
    }

    public void setSecondTime(String time) {
        PMSUtil.setSecondTime(mContext, time);
    }

    public Boolean getAllowedTime() {
        return PMSUtil.getAllowedTime(mContext);
    }

    public void setAllowedTime(Boolean flag) {
        PMSUtil.setAllowedTime(mContext, flag);
    }

    public void setIsPopupActivity(Boolean ispopup) {
        PMSUtil.setPopupActivity(mContext, ispopup);
    }

    public void setNotiOrPopup(Boolean isnotiorpopup) {
        PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
    }

    public void IsCustNoti(Boolean isCustNoti) {
        PMSUtil.setIsCustNoti(mContext, isCustNoti);
    }

    public void setCustNotiResId(int resId) {
        PMSUtil.setNotiResId(mContext, resId);
    }

    public void setCustNotiTitleResId(int resId) {
        PMSUtil.setNotiTitleResId(mContext, resId);
    }

    public void setCustNotiContentResId(int resId) {
        PMSUtil.setNotiContentResId(mContext, resId);
    }

    /**
     * set inbox activity
     *
     * @param inboxActivity
     */
    public void setInboxActivity(String inboxActivity) {
        mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
    }

    /**
     * set push popup activity
     *
     * @param classPath
     */
    public void setPushPopupActivity(String classPath) {
        mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
    }

    /**
     * set pushPopup showing time
     *
     * @param pushPopupShowingTime
     */
    public void setPushPopupShowingTime(int pushPopupShowingTime) {
        mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
    }

    /**
     * get msg flag
     *
     * @return
     */
    public String getMsgFlag() {
        return mPrefs.getString(PREF_MSG_FLAG);
    }

    /**
     * get noti flag
     *
     * @return
     */
    public String getNotiFlag() {
        return mPrefs.getString(PREF_NOTI_FLAG);
    }

    /**
     * get dup flag
     *
     * @return
     */
    public String getDupFlag() {
        return mPrefs.getString(PREF_DUP_FLAG);
    }

    /**
     * set large noti icon
     *
     * @param resId
     */
    public void setLargeNotiIcon(int resId) {
        mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
    }

    /**
     * set noti cound
     *
     * @param resId
     */
    public void setNotiSound(int resId) {
        mPrefs.putInt(PREF_NOTI_SOUND, resId);
    }

    public void setNotificationClickReceiver(String action, Class broadcastReceiver) {
        mPrefs.putString(PREF_NOTI_RECEIVER_ACTION, action);
        mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, broadcastReceiver.getCanonicalName());
    }
    public void setNotificationEventReceiver(String action, Class broadcastReceiver){
        mPrefs.putString(PREF_PUSH_RECEIVER_ACTION, action);
        mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, broadcastReceiver.getCanonicalName());
    }

	public void setNotificationClickActivityClass(String action, Class activity) {
		PMSUtil.setNotificationClickActivityAction(mContext, action);
		PMSUtil.setNotificationClickActivityClass(mContext, activity);
		PMSUtil.setNotificationClickActivityUseBackStack(mContext, false);
	}

	public void setNotificationClickActivityUseBackStack(String action, Class activity) {
		PMSUtil.setNotificationClickActivityAction(mContext, action);
		PMSUtil.setNotificationClickActivityClass(mContext, activity);
		PMSUtil.setNotificationClickActivityUseBackStack(mContext, true);
	}

	public void setNotificationClickActivityFlag(int flags) {
		PMSUtil.setNotificationClickActivityFlag(mContext, flags);
	}

	public void setNotificationClickActivityFlagEnable(boolean value) {
		PMSUtil.setNotificationClickActivityFlagEnable(mContext, value);
	}

	public void createNotificationChannel(){
		PMSUtil.createNotificationChannel(mContext);
	}

    @Deprecated
    public void setNotiReceiver(String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER_ACTION, intentAction);
    }

    @Deprecated
    public void setNotiReceiverClass(String intentClass) {
        mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentClass);
    }

    @Deprecated
    public void setPushReceiverClass(String intentClass) {
        mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentClass);
    }

    public String getNotiStackEnable() {
        return mPrefs.getString(IPMSConsts.PREF_NOTI_STYLE_STACK);
    }

    public void setNotiStackEnable(boolean isStack) {
        mPrefs.putString(PREF_NOTI_STYLE_STACK, isStack ? "Y" : "N");
    }

    /**
     * set ring mode
     *
     * @param isRingMode
     */
    public void setRingMode(boolean isRingMode) {
        mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
    }

    /**
     * set vibe mode
     *
     * @param isVibeMode
     */
    public void setVibeMode(boolean isVibeMode) {
        mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
    }

    /**
     * set popup noti
     *
     * @param isShowPopup
     */
    public void setPopupNoti(boolean isShowPopup) {
        mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
    }

    /**
     * set screen wakeup
     *
     * @param isScreenWakeup
     */
    public void setScreenWakeup(boolean isScreenWakeup) {
        mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
    }


    public OnReceivePushListener getOnReceivePushListener() {
        return onReceivePushListener;
    }

    public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener) {
        this.onReceivePushListener = onReceivePushListener;
    }

    /**
     * set debugTag
     *
     * @param tagName
     */
    public void setDebugTAG(String tagName) {
        CLog.setTagName(tagName);
    }

    /**
     * set debug mode
     *
     * @param debugMode
     */
    public void setDebugMode(boolean debugMode) {
        CLog.setDebugMode(mContext, debugMode);
    }

    public void bindBadge(Context c, int id) {
//        Badge.getInstance(c).addBadge(c, id);
    }

    public void bindBadge(TextView badge) {
//        Badge.getInstance(badge.getContext()).addBadge(badge);
    }

    /**
     * show Message Box
     *
     * @param c
     */
    public void showMsgBox(Context c) {
        showMsgBox(c, null);
    }

    public void showMsgBox(Context c, Bundle extras) {
        CLog.i("showMsgBox");
        if (PhoneState.isAvailablePush()) {
            try {
                Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

                Intent i = new Intent(mContext, inboxActivity);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (extras != null) {
                    i.putExtras(extras);
                }
                c.startActivity(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Intent i = new Intent(INBOX_ACTIVITY);
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // if (extras != null) {
            // i.putExtras(extras);
            // }
            // c.startActivity(i);
        }
    }

    public void closeMsgBox(Context c) {
        Intent i = new Intent(RECEIVER_CLOSE_INBOX);
        c.sendBroadcast(i);
    }

    public void sendGoBackNoti(Context c)
    {
        Intent i = new Intent(c, PushReceiver.class);
        i.setAction(PushReceiver.BADGE_ACTION);
        i.putExtra(PushReceiver.BADGE_TYPE, PushReceiver.BADGE_TYPE_CLEAR);

        c.sendBroadcast(i);
    }

    public void clearLauncherBadgeValue(boolean clearAllOfNotification)
    {
        Intent i = new Intent(mContext, PushReceiver.class);
        i.setAction(PushReceiver.BADGE_ACTION);
        i.putExtra(PushReceiver.BADGE_TYPE, PushReceiver.BADGE_TYPE_CLEAR);
        i.putExtra(PushReceiver.BADGE_EXTRA, clearAllOfNotification);
        mContext.sendBroadcast(i);
    }

//    public void setLauncherBadgeValue(int value)
//    {
//        Intent i = new Intent(mContext, PushReceiver.class);
//        i.setAction(PushReceiver.BADGE_ACTION);
//        i.putExtra(PushReceiver.BADGE_TYPE, PushReceiver.BADGE_TYPE_UPDATE);
//        i.putExtra(PushReceiver.BADGE_EXTRA, value);
//        mContext.sendBroadcast(i);
//    }

    public int getLauncherBadgeValue()
    {
        Prefs prefs = new Prefs(mContext);
        return prefs.getInt(IPMSConsts.PREF_LAUNCHER_BADGE_COUNT);
    }

    /*
     * ===================================================== [start] database =====================================================
     */

    /**
     * test
     */
    //public void insertMsgTest(int cnt) { mDB.testInsertMsgGrp("9999", cnt); }

    /**
     * select MsgGrp list
     *
     * @return
     */
    public Cursor selectMsgGrpList() {
        return mDB.selectMsgGrpList();
    }

    /**
     * select MsgGrp
     *
     * @param msgCode
     * @return
     */
    public MsgGrp selectMsGrp(String msgCode) {
        return mDB.selectMsgGrp(msgCode);
    }

    /**
     * select new msg Cnt
     *
     * @return
     */
    public int selectNewMsgCnt() {
        return mDB.selectNewMsgCnt();
    }

    /**
     * select grp new msg Cnt
     *
     * @return
     */
    public int selectGrpNewMsgCnt(String msgGrpCd) {
        return mDB.selectGrpNewMsgCnt(msgGrpCd);
    }

    /**
     * selectMsgList
     *
     * @param page 페이지
     * @param row  갯수
     * @return Cursor
     */
    public Cursor selectMsgList(int page, int row) {
        return mDB.selectMsgList(page, row);
    }

    /**
     * selectMsgList
     *
     * @param page     페이지
     * @param row      갯수
     * @param sortType 정렬종류 [
     *                 1. UID 순방향 : {@link #SORT_UID_ASC},
     *                 2. UID 역방향 : {@link #SORT_UID_DESC},
     *                 3. UID 순방향, REG_DATE 정렬 제외 : {@link #SORT_UID_NO_REGDATE_ASC},
     *                 4. UID 역방향, REG_DATE 정렬 제외 : {@link #SORT_UID_NO_REGDATE_DESC}]
     * @return Cursor
     */
    public Cursor selectMsgList(int page, int row, int sortType) {
        return mDB.selectMsgList(page, row, sortType);
    }

    /**
     * selectMsgListWithSubStr
     *
     * @param page     페이지
     * @param row      갯수
     * @param sortType 정렬종류 [
     *                 1. UID(앞에서부터 n) 순방향 : {@link #SORT_SUBSTR_UID_ASC},
     *                 2. UID(앞에서부터 n) 역방향 : {@link #SORT_SUBSTR_UID_DESC},
     *                 3. UID(뒤에서부터 n) 순방향 : {@link #SORT_FISRT_SUBSTR_UID_ASC},
     *                 4. UID(뒤에서부터 n) 역방향 : {@link #SORT_FISRT_SUBSTR_UID_DESC},
     *                 5. UID(앞에서부터 n) 순방향, REG_DATE 정렬 제외 : {@link #SORT_SUBSTR_UID_NO_REGDATE_ASC},
     *                 6. UID(앞에서부터 n) 역방향, REG_DATE 정렬 제외 : {@link #SORT_SUBSTR_UID_NO_REGDATE_DESC},
     *                 7. UID(뒤에서부터 n) 순방향, REG_DATE 정렬 제외 : {@link #SORT_FISRT_SUBSTR_UID_NO_REGDATE_ASC},
     *                 8. UID(뒤에서부터 n) 역방향, REG_DATE 정렬 제외 : {@link #SORT_FISRT_SUBSTR_UID_NO_REGDATE_DESC}]
     * @param length   n
     * @return Cursor
     */
    public Cursor selectMsgListWithSubStr(int page, int row, int sortType, int length) {
        return mDB.selectMsgListWithSubStr(page, row, sortType, length);
    }

    /**
     * selectMsgList
     *
     * @param msgCode 메세지코드
     * @return Cursor
     */
    public Cursor selectMsgList(String msgCode) {
        return mDB.selectMsgList(msgCode);
    }

    /**
     * selectMsgList
     *
     * @param msgCode  메세지코드
     * @param sortType 정렬종류 [
     *                 1. UID 순방향 : {@link #SORT_UID_ASC},
     *                 2. UID 역방향 : {@link #SORT_UID_DESC},
     *                 3. UID 순방향, REG_DATE 정렬 제외 : {@link #SORT_UID_NO_REGDATE_ASC},
     *                 4. UID 역방향, REG_DATE 정렬 제외 : {@link #SORT_UID_NO_REGDATE_DESC}]
     * @return Cursor
     */
    public Cursor selectMsgList(String msgCode, int sortType) {
        return mDB.selectMsgList(msgCode, sortType);
    }

    public Cursor selectMsgListFromLocation(int page, int row, int cntSubLength) {
        return mDB.selectMsgListFromLocation(cntSubLength, page, row);
    }

    public Cursor selectMsgListFromLocation(int cntSubLength) {
        return mDB.selectMsgListFromLocation(cntSubLength, -1, -1);
    }
    
    /**
     * selectMsgListWithSubStr
     *
     * @param msgCode  메세지코드
     * @param sortType 정렬종류 [
     *                 1. UID(앞에서부터 n) 순방향 : {@link #SORT_SUBSTR_UID_ASC},
     *                 2. UID(앞에서부터 n) 역방향 : {@link #SORT_SUBSTR_UID_DESC},
     *                 3. UID(뒤에서부터 n) 순방향 : {@link #SORT_FISRT_SUBSTR_UID_ASC},
     *                 4. UID(뒤에서부터 n) 역방향 : {@link #SORT_FISRT_SUBSTR_UID_DESC},
     *                 5. UID(앞에서부터 n) 순방향, REG_DATE 정렬 제외 : {@link #SORT_SUBSTR_UID_NO_REGDATE_ASC},
     *                 6. UID(앞에서부터 n) 역방향, REG_DATE 정렬 제외 : {@link #SORT_SUBSTR_UID_NO_REGDATE_DESC},
     *                 7. UID(뒤에서부터 n) 순방향, REG_DATE 정렬 제외 : {@link #SORT_FISRT_SUBSTR_UID_NO_REGDATE_ASC},
     *                 8. UID(뒤에서부터 n) 역방향, REG_DATE 정렬 제외 : {@link #SORT_FISRT_SUBSTR_UID_NO_REGDATE_DESC}]
     * @param length   n
     * @return Cursor
     */
    public Cursor selectMsgListWithSubStr(String msgCode, int sortType, int length) {
        return mDB.selectMsgListWithSubStr(msgCode, sortType, length);
    }

    /**
     * select Msg
     *
     * @param msgId
     * @return
     */
    public Msg selectMsgWhereMsgId(String msgId) {
        return mDB.selectMsgWhereMsgId(msgId);
    }

    /**
     * select Msg
     *
     * @param userMsgID
     * @return
     */
    public Msg selectMsgWhereUserMsgId(String userMsgID) {
        return mDB.selectMsgWhereUserMsgId(userMsgID);
    }

    /**
     * select query
     *
     * @param sql
     * @return
     */
    public Cursor selectQuery(String sql) {
        return mDB.selectQuery(sql);
    }

    /**
     * update MsgGrp
     *
     * @param msgCode
     * @param values
     * @return
     */
    public long updateMsgGrp(String msgCode, ContentValues values) {
        return mDB.updateMsgGrp(msgCode, values);
    }

    /**
     * update read msg
     *
     * @param msgGrpCd
     * @param firstUserMsgId
     * @param lastUserMsgId
     * @return
     */
    public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
        return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
    }

    /**
     * update read msg where userMsgId
     *
     * @param userMsgId
     * @return
     */
    public long updateReadMsgWhereUserMsgId(String userMsgId) {
        return mDB.updateReadMsgWhereUserMsgId(userMsgId);
    }

    /**
     * update read msg where msgId
     *
     * @param msgId
     * @return
     */
    public long updateReadMsgWhereMsgId(String msgId) {
        return mDB.updateReadMsgWhereMsgId(msgId);
    }

    /**
     * delete Msg
     *
     * @param userMsgId
     * @return
     */
    public long deleteUserMsgId(String userMsgId) {
        return mDB.deleteUserMsgId(userMsgId);
    }

    /**
     * delete Msg
     *
     * @param MsgId
     * @return
     */
    public long deleteMsgId(String MsgId) {
        return mDB.deleteMsgId(MsgId);
    }

    /**
     * delete MsgGrp
     *
     * @param msgCode
     * @return
     */
    public long deleteMsgGrp(String msgCode) {
        return mDB.deleteMsgGrp(msgCode);
    }

    /**
     * delete expire Msg
     *
     * @return
     */
    public long deleteExpireMsg() {
        return mDB.deleteExpireMsg();
    }

    /**
     * delete empty MsgGrp
     *
     * @return
     */
    public void deleteEmptyMsgGrp() {
        mDB.deleteEmptyMsgGrp();
    }

    /**
     * delete all
     */
    public void deleteAll() {
        mDB.deleteAll();
    }

    /*
     * ===================================================== [end] database =====================================================
     */

    public int getAllOfUnreadCount() {return mDB.getAllOfUnreadMsgCount();}

    public int getUnreadCountByMsgGrpCode(String msgGrpCode) {return mDB.getUnreadMsgCountByMsgGrpCode(msgGrpCode);}
    public Cursor selectAllOfMsg () {
        return mDB.selectAllOfMsg();
    }
    public Cursor selectAllOfMsgByMsgGrpCode (String msgGrpCode) {
        return mDB.selectAllOfMsgByMsgGrpCode(msgGrpCode);
    }
    public Cursor selectAllOfMsgGrp () {
        return mDB.selectAllOfMsgGrp();
    }

    @RequiresApi(26)
    public void setNotificationChannelInfo(String name, String description)
    {
        if(name != null)
        {
            mPrefs.putString(PREF_NOTIFICATION_CHANNEL_NAME, name);
        }
        if(description != null)
        {
            mPrefs.putString(PREF_NOTIFICATION_CHANNEL_DESCRIPTION, description);
        }
        NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = DataKeyUtil.getDBKey(mContext, DB_NOTI_CHANNEL_ID);
        NotificationChannel channel = manager.getNotificationChannel(channelId);
        if(channel != null)
        {
            if(name != null)
            {
                channel.setName(name);
            }
            if(description != null)
            {
                channel.setDescription(description);
            }
            manager.createNotificationChannel(channel);
        }
    }

    @RequiresApi(26)
    public void setDoNotDisturbChannelInfo(String name, String description)
    {
        if(name != null)
        {
            mPrefs.putString(PREF_DONOTDISTURB_CHANNEL_NAME, name);
        }
        if(description != null)
        {
            mPrefs.putString(PREF_DONOTDISTURB_CHANNEL_DESCRIPTION, description);
        }
        NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = manager.getNotificationChannel(NOTIFICATION_CHANNEL_DO_NOT_DISTURB);
        if(channel != null)
        {
            if(name != null)
            {
                channel.setName(name);
            }
            if(description != null)
            {
                channel.setDescription(description);
            }
            manager.createNotificationChannel(channel);
        }
    }
}
